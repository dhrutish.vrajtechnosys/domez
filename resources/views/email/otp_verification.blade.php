<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
    <div style="margin:50px auto;width:70%;padding:20px 0">
        <div style="border-bottom:1px solid #eee">
            <a href="" style="font-size:1.4em;color: #00bac7;text-decoration:none;font-weight:600">
                <img src="{{$logo}}" alt="Logo">
            </a>
        </div>
        <p style="font-size:1.1em">Hi, {{$name}}</p>
        <p>Thank you for choosing Domez. Use the following OTP to verify your email address.</p>
        <h2 style="background: #00bac7;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">{{$otp}}</h2>
        <p style="font-size:0.9em;">Regards,<br />Domez</p>
        <hr style="border:none;border-top:1px solid #eee" />
    </div>
</div>
