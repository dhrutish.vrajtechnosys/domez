<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Page Title -->
    <title>{{ trans('labels.domez') }}</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ Helper::image_path('preloader.gif') }}" sizes="any">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/bootstrap/bootstrap.min.css') }}">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/fontawesome/all.min.css') }}">
    <!-- Sweetalert CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/sweetalert/sweetalert2.min.css') }}">
    <!-- Toastr CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/toastr/toastr.min.css') }}">
    <!-- slick Css -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/slick/slick.css') }}">
    <!-- animate compact Css -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/animate.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/landing.css') }}">
    <!-- responsive Css -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/responsive.css') }}">
</head>

<body>

    <div class="layout">
        <header class="container header-section">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand logo-img" href="#"><img src="{{ url('storage/app/public/new_admin/images/landing/logo.png') }}" /></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mr-4">
                        <li class="nav-item mx-3">
                            <a class="nav-link" href="#">PRIVACY & POLICY</a>
                        </li>
                        <li class="nav-item mx-3">
                            <a class="nav-link" href="#">TERMS OF SERVICE</a>
                        </li>
                        <li class="nav-item mx-3">
                            <a class="nav-link" href="#">FAQ</a>
                        </li>
                    </ul>

                    <a href="{{URL::to('login')}}" class="btn btn-success dmz-button px-4 my-2 my-sm-0">Sign In</a>
                </div>
            </nav>
        </header>
        <section class="main-banner">
            <div class="container">
                <div class="banner-content wow fadeInDown animated animated" data-wow-duration="2s">
                    <span class="text-uppercase fs-5">welcome to domez</span>
                    <h2 class="text-capitalize">the most advanced booking system for your dome</h2>
                    <p class="text-capitalize fs-5 fw-semibold">get bookings faster than ever before through the DOMEZ mobile application!</p>
                    <button class="btn btn-success dmz-button px-4">Get Started</button>
                </div>
                <div class="banner-images wow fadeInDown animated animated">
                    <img class="w-100" src="{{url('storage/app/public/new_admin/images/landing/OBJECTS.png')}}" />
                </div>
            </div>
        </section>
        <section class="properly-games small-padding">
            <div class="container">
                <h1 class="heading-title wow fadeInDown delay-0-2s animated">Most Popular Sports</h1>
                <div class="row py-5">
                    <div class="col-lg-3 col-md-6">
                        <div class="most-games-box position-relative" data-wow-duration="1s">
                            <div class="game-info m-auto">
                                <img src="{{url('storage/app/public/new_admin/images/landing/game-1.png')}}" />
                                <h5>Soccer</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="most-games-box position-relative" data-wow-duration="1s">
                            <div class="game-info m-auto">
                                <img src="{{url('storage/app/public/new_admin/images/landing/game-2.png')}}" />
                                <h5>Volleyball</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="most-games-box position-relative" data-wow-duration="1s">
                            <div class="game-info m-auto">
                                <img src="{{url('storage/app/public/new_admin/images/landing/game-3.png')}}" />
                                <h5>Tennis</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="most-games-box position-relative" data-wow-duration="1s">
                            <div class="game-info m-auto">
                                <img src="{{url('storage/app/public/new_admin/images/landing/game-4.png')}}" />
                                <h5>Golf</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="game-details">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 right-column m-0 align-items-center text-md-start text-center ps-3 pe-3">
                        <div class="game-details-img" data-wow-duration="2s">
                            <img class="w-100"
                                src="{{ url('storage/app/public/new_admin/images/landing/game-play-1.png') }}" />
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 right-column m-0 align-items-center text-md-start  ps-3 pe-3">
                        <div class="game-details-text" data-wow-duration="2s">
                            <h2 class="wow fadeInDown  delay-0-2s animated">Monitor Your Revenue, Number of Bookings and Number of Users Daily.</h2>
                            <p>Domez provides a complete suite of tools to manage your bookings and payments processes. At the centre of everything we do are the sports facilities themselves.</p>
                            <p>By their very nature, sports facilities vary based on a wide range of factors. Location, opening times, typically booking slot lengths, facility types and much more.</p>
                            <p>At Domez, our sole focus is an online platform for sports facilities. This allows us to focus on creating a platform that caters to the specific requirements of pitches, courts, halls and other playing surfaces.</p>
                            <button class="btn btn-success wow fadeInRight delay-0-2s animated">Get Started</button>
                        </div>
                    </div>
                </div>
                <div class="row game-play align-items-center pt-md-5 pt-sm-4">
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 right-column m-0 align-items-center text-md-start  ps-3 pe-3">
                        <div class="game-details-text" data-wow-duration="2s">
                            <h2 class="wow fadeInDown  delay-0-2s animated">Domez Solved the Problem!</h2>
                            <p>Now each player can pay their portion since the app splits the amount among the number of players equally through shared access to the booking. No more the hassle of paying back and missed calculations after each match. An option to pay in full is also offered if one player would like to pay for the whole booking fee.</p>
                            <button class="btn btn-success dmz-button wow fadeInRight delay-0-2s animated">Get Started</button>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 right-column m-0 align-items-center text-md-start text-center ps-3 pe-3">
                        <div class="game-details-img game-details-img" data-wow-duration="2s">
                            <img class="w-100"
                                src="{{ url('storage/app/public/new_admin/images/landing/game-play-2.png') }}" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="book-sports">
            <div class="container">
                <div class="row align-items-center ">
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 right-column m-0 align-items-center text-md-start text-center ps-3 pe-3">
                        <div class="game-details-img" data-wow-duration="2s">
                            <img class="w-100"
                                src="{{ url('storage/app/public/new_admin/images/landing/Book.png') }}" />
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 right-column m-0 align-items-center text-md-start  ps-3 pe-3">
                        <div class="game-details-text" data-wow-duration="2s">
                            <h2 class="wow fadeInDown  delay-0-2s animated">Book a sports<br>facility near you</h2>
                            <p>At the Domez , we can offer your team a personalized playing experience due to our adaptable playing surface. With outfield fences for Volleyball, and Soccer Goals, and Tennis, and Golf, the Domez can easily be customized to fit your specific needs.</p>
                            <p>Contact us today, and we can take you through the<br>set-ups we can provide!</p>
                            <button class="btn btn-success dmz-button wow fadeInRight delay-0-2s animated">Get Started</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="testimonials-section">
            <div class="theme-content-area">
                <div class="testimonials-wrapper">
                    <div class="container">
                        <div class="row flex-center align-items-center">
                                <div class="testimonia-slider-wrapper animation-element fadeIn in-view">
                                    <ul class="testimonial-slider slick-initialized slick-slider slick-vertical"><button class="slick-prev slick-arrow" aria-label="Previous" type="button" style="">Previous</button>
                            <div class="slick-list" style="height: 452px; padding: 70px 0px;"><div class="slick-track" style="opacity: 1; height: 4228px; transform: translate3d(0px, -2114px, 0px);"><li data-testimonial="4" class="slick-slide slick-cloned" data-slick-index="-2" id="jquery-slick-js" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                <div class="client-testimonial-wrapper">
                                    <div class="client-testimonial-content">
                                        <div class="client-img">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                        </div>
                                        <div class="client-infomation">
                                            <div class="client-name">Eddie Jacobs</div>
                                            <div class="client-designation">Hocky Power</div>
                                        </div>
                                        <div class="client-feedback">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                        </div>
                                    </div>
                                </div>
                                </li><li data-testimonial="5" class="slick-slide slick-cloned slick-center" data-slick-index="-1" id="jquery-slick-js" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                <div class="client-testimonial-wrapper">
                                    <div class="client-testimonial-content">
                                        <div class="client-img">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                        </div>
                                        <div class="client-infomation">
                                            <div class="client-name">Eddie Jacobs</div>
                                            <div class="client-designation">Hocky Power</div>
                                        </div>
                                        <div class="client-feedback">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                        </div>
                                    </div>
                                </div>
                                </li><li data-testimonial="0" class="slick-slide" data-slick-index="0" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                <div class="client-testimonial-wrapper">
                                    <div class="client-testimonial-content">
                                        <div class="client-img">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                        </div>
                                        <div class="client-infomation">
                                            <div class="client-name">Eddie Jacobs</div>
                                            <div class="client-designation">Hocky Power</div>
                                        </div>
                                        <div class="client-feedback">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                        </div>
                                    </div>
                                </div>
                                </li><li data-testimonial="1" class="slick-slide" data-slick-index="1" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                <div class="client-testimonial-wrapper">
                                    <div class="client-testimonial-content">
                                        <div class="client-img">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                        </div>
                                        <div class="client-infomation">
                                            <div class="client-name">Eddie Jacobs</div>
                                            <div class="client-designation">Hocky Power</div>
                                        </div>
                                        <div class="client-feedback">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                        </div>
                                    </div>
                                </div>
                                </li><li data-testimonial="2" class="slick-slide" data-slick-index="2" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                <div class="client-testimonial-wrapper">
                                    <div class="client-testimonial-content">
                                        <div class="client-img">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                        </div>
                                        <div class="client-infomation">
                                            <div class="client-name">Eddie Jacobs</div>
                                            <div class="client-designation">Hocky Power</div>
                                        </div>
                                        <div class="client-feedback">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                        </div>
                                    </div>
                                </div>
                                </li><li data-testimonial="3" class="slick-slide" data-slick-index="3" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                <div class="client-testimonial-wrapper">
                                    <div class="client-testimonial-content">
                                        <div class="client-img">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                        </div>
                                        <div class="client-infomation">
                                            <div class="client-name">Eddie Jacobs</div>
                                            <div class="client-designation">Hocky Power</div>
                                        </div>
                                        <div class="client-feedback">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                        </div>
                                    </div>
                                </div>
                                </li><li data-testimonial="4" class="slick-slide" data-slick-index="4" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                <div class="client-testimonial-wrapper">
                                     <div class="client-testimonial-content">
                                        <div class="client-img">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                        </div>
                                        <div class="client-infomation">
                                            <div class="client-name">Eddie Jacobs</div>
                                            <div class="client-designation">Hocky Power</div>
                                        </div>
                                        <div class="client-feedback">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                        </div>
                                    </div>
                                </div>
                                </li><li data-testimonial="5" class="slick-slide slick-current slick-active slick-center" data-slick-index="5" aria-hidden="false" style="width: 800px;" tabindex="0">
                                    <div class="client-testimonial-wrapper">
                                        <div class="client-testimonial-content">
                                            <div class="client-img">
                                                <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                            </div>
                                            <div class="client-infomation">
                                                <div class="client-name">Eddie Jacobs</div>
                                                <div class="client-designation">Hocky Power</div>
                                            </div>
                                            <div class="client-feedback">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li><li data-testimonial="0" class="slick-slide slick-cloned nextSlide1" data-slick-index="6" id="jquery-slick-js" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                    <div class="client-testimonial-wrapper">
                                        <div class="client-testimonial-content">
                                            <div class="client-img">
                                                <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                            </div>
                                            <div class="client-infomation">
                                                <div class="client-name">Eddie Jacobs</div>
                                                <div class="client-designation">Hocky Power</div>
                                            </div>
                                            <div class="client-feedback">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li><li data-testimonial="1" class="slick-slide slick-cloned nextSlide2" data-slick-index="7" id="jquery-slick-js" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                    <div class="client-testimonial-wrapper">
                                        <div class="client-testimonial-content">
                                            <div class="client-img">
                                                <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                            </div>
                                            <div class="client-infomation">
                                                <div class="client-name">Eddie Jacobs</div>
                                                <div class="client-designation">Hocky Power</div>
                                            </div>
                                            <div class="client-feedback">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li><li data-testimonial="2" class="slick-slide slick-cloned" data-slick-index="8" id="jquery-slick-js" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                    <div class="client-testimonial-wrapper">
                                        <div class="client-testimonial-content">
                                            <div class="client-img">
                                                <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                            </div>
                                            <div class="client-infomation">
                                                <div class="client-name">Eddie Jacobs</div>
                                                <div class="client-designation">Hocky Power</div>
                                            </div>
                                            <div class="client-feedback">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li><li data-testimonial="3" class="slick-slide slick-cloned" data-slick-index="9" id="jquery-slick-js" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                    <div class="client-testimonial-wrapper">
                                        <div class="client-testimonial-content">
                                            <div class="client-img">
                                                <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                            </div>
                                            <div class="client-infomation">
                                                <div class="client-name">Eddie Jacobs</div>
                                                <div class="client-designation">Hocky Power</div>
                                            </div>
                                            <div class="client-feedback">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li><li data-testimonial="4" class="slick-slide slick-cloned" data-slick-index="10" id="jquery-slick-js" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                    <div class="client-testimonial-wrapper">
                                        <div class="client-testimonial-content">
                                            <div class="client-img">
                                                <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                            </div>
                                            <div class="client-infomation">
                                                <div class="client-name">Eddie Jacobs</div>
                                                <div class="client-designation">Hocky Power</div>
                                            </div>
                                            <div class="client-feedback">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li><li data-testimonial="5" class="slick-slide slick-cloned" data-slick-index="11" id="jquery-slick-js" aria-hidden="true" style="width: 800px;" tabindex="-1">
                                    <div class="client-testimonial-wrapper">
                                        <div class="client-testimonial-content">
                                            <div class="client-img">
                                                <img src="{{url('storage/app/public/new_admin/images/landing/Eddie-Jacobs.png')}}" />
                                            </div>
                                            <div class="client-feedback">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh in turpis. Consequat duis diam lacus arcu. Faucibus venenatis felis id augue sit cursus pellentesque enim Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh mauris, nec turpis orci lectus maecenas. Suspendisse.</p>
                                            </div>
                                            <div class="client-infomation">
                                                <div class="client-name">Eddie Jacobs</div>
                                                <div class="client-designation">Hocky Power</div>
                                            </div>
                                        </div>
                                    </div>
                                         </li>
                                    </div>
                                </div>
                                     <button class="slick-next slick-arrow" id="" aria-label="Next" type="button" style="">Next</button></ul>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="contact-form">
            <div class="container">
                <div class="row contact-form align-items-center">
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pt-sm-5">
                        <div class="game-details-text" data-wow-duration="2s">
                            <h2 class="wow fadeInDown  delay-0-2s animated">Do you have any questions?
                                <br>We are here to help!</h2>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <form class="form-wrapper">
                                        <div class="d-flex p-0">
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pe-xxl-4 form-control-responsive pe-sm-0 mb-sm-3">
                                                <label>Name</label>
                                                <input type="text" placeholder="Enter Name" placeholder="Enter Name" label for="Enter Name" class="form-control w-100 py-3 border-1"/>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12  form-control-responsive pe-sm-0 mb-sm-3 ps-0">
                                                <label>Email</label>
                                                <input type="text" placeholder="Enter Email"  class="form-control w-100 p-3 border-1"/>
                                             </div>
                                        </div>
                                        <div class="d-flex p-0">
                                            <div class="col-lg-12 col-xl-12	col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3 mb-3 ps-0">
                                                <label>Subject *</label>
                                                <input type="Enter Subject" placeholder="Enter Subject" class="form-control w-100 py-3 border-1"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-xl-12	col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3 mb-4 ps-0">
                                            <label>Massage</label>
                                            <textarea  placeholder="Enter Message" rows="4" class="form-control  p-3 border-1"></textarea>
                                        </div>
                                    </form>
                                </div>
                            <button class="btn btn-success dmz-button wow fadeInRight delay-0-2s animated">Send your message</button>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="game-details-img" data-wow-duration="2s">
                            <img class="w-100"
                                src="{{ url('storage/app/public/new_admin/images/landing/contact.png') }}" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="faq-quection">
            <div class="container">
                <div class="row ">
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                        <div class="faq-frequently-one">
                            <img src="{{ url('storage/app/public/new_admin/images/landing/fram-1.png') }}" />
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                        <div class="game-details-text" data-wow-duration="2s">
                            <h2 class="wow fadeInDown  delay-0-2s animated">frequently asked questions</h2>
                        </div>
                        <div class="row d-flex justify-content-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 w-100 mt-4">
                                <div class="accordion wow fadeInDown delay-0-2s animated" id="accordionExample">
                                    <div class="accordion-item">
                                      <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Can i watch local sports in my are?
                                        </button>
                                      </h2>
                                      <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not..</p>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="accordion-item">
                                      <h2 class="accordion-header" id="headingTwo">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Can i sign in to watchESPN, fox sports go or NBC Sports ?
                                        </button>
                                      </h2>
                                      <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not..</p>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="accordion-item">
                                      <h2 class="accordion-header" id="headingThree">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            What is the video qualitu and how much bandwidth doi need ?
                                        </button>
                                      </h2>
                                      <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not..</p>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="headingFour">
                                          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            How can i Stream sports on multiple devices at the same time ?
                                          </button>
                                        </h2>
                                        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                          <div class="accordion-body">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not..</p>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                        <div class="faq-frequently">
                            <img src="{{ url('storage/app/public/new_admin/images/landing/frame-2.png') }}" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer start -->
		<footer class=" footer-text footer-section">
			<div class="container">
				<div class="row footer-content">
					<div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 ps-sm-0">
						<div class="footer-box wow fadeInDown delay-0-4s animated">
							<div class="logo-img mb-4 w-100">
								<a href="index.html"><img class="footer-logo" src="{{ url('storage/app/public/new_admin/images/landing/Domez-Logo-name-white.png') }}" / ></a>
							</div>
							<p class="footer-description pe-5 me-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore agna.Lorem ipsum dolor sit amet. Lorem ipsum dolor.</p>
                            <div class="share-link-icon">
                                <ul class="social-icon d-flex list-unstyled">
                                    <li class="socail-space me-2"><a href="#" class="social"><i class="fa-brands fa-facebook-f"></i></a></li>
                                    <li class="socail-space mx-2"><a href="#" class="social"><i class="fa-brands fa-twitter"></i></a></li>
                                    <li class="socail-space mx-2"><a href="#" class="social"><i class="fa-brands fa-instagram"></i></a></li>
                                    <li class="socail-space mx-2"><a href="#" class="social"><i class="fa-brands fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
					</div>
					<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 wow fadeInDown delay-0-4s animated">
						<h6 class="fw-Medium mb-4">Support</h6>
						<div class="footer-link link">
							<p>
							   <a href="#" class="text-reset text-copyright mb-2"> Privacy & Policy</a>
							</p>
							<p>
							   <a href="#" class="text-reset text-copyright">Terms Of Service</a>
							</p>
							<p>
							    <a href="#" class="text-reset text-copyright">FAQ</a>
							</p>
						</div>
					</div>
					<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 pt-3 ps-sm-0 pt-sm-3 footer-form-content wow fadeInDown delay-0-4s animated">
						<h6 class="fw-Medium mb-4">Available on</h6>
						<div class="footer-form">
							<div class="icon-link d-flex align-items-center mb-3">
                                <img src="{{ url('storage/app/public/new_admin/images/landing/google-play.png') }}" />
							</div>
							<div class="icon-link d-flex align-items-center mb-4">
                                <img src="{{ url('storage/app/public/new_admin/images/landing/apple-store.png') }}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- Footer end -->
		<!--  footer bottom start -->
		<div class="footer-bottom-section text-center">
			<div class="container border-top">
				<div class="row">
					<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 footer-bottom">
						<p class="copright-text m-0 py-3"><a href="index.html" class="text-decoration-none text-copyright wow fadeInLeft delay-0-2s animated">Copyright © 2023. All rights reserved.</a></p>
					</div>
				</div>
			</div>
		</div>
		<!-- footer  bottom  end -->
        {{-- <section class="small-padding">
            <div class="container">
                <div class="row my-5">
                    <div class="col-lg-4">
                        <div class="features-images">
                            <img class="w-100"
                                src="{{ url('storage/app/public/new_admin/images/landing/features-img.png') }}" />
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="features-content">
                            <div class="hedding-title mb-5">
                                <h3 class="text-left">Features</h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="features-box">
                                        <div class="icon">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/features-icon-1.png')}}" />
                                        </div>
                                        <h4>Golf at Bingo</h4>
                                        <p>
                                            Rorem ipsum dolor sit ametconsectetur adipiscing elit,
                                            sed do eiusmod
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="features-box">
                                        <div class="icon">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/features-icon-2.png')}}" />
                                        </div>
                                        <h4>More than 320 Acres</h4>
                                        <p>
                                            Sorem ipsum dolor sit ametconsectetur adipiscing elit,
                                            sed do eiusmod
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="features-box">
                                        <div class="icon">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/features-icon-3.png')}}" />
                                        </div>
                                        <h4>Events</h4>
                                        <p>
                                            Gorem ipsum dolor sit ametconsectetur adipiscing elit,
                                            sed do eiusmod
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="features-box">
                                        <div class="icon">
                                            <img src="{{url('storage/app/public/new_admin/images/landing/features-icon-4.png')}}" />
                                        </div>
                                        <h4>Activites</h4>
                                        <p>
                                            Horem ipsum dolor sit ametconsectetur adipiscing elit,
                                            sed do eiusmod
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        {{-- <section class="testimonial-section small-padding">
            <div class="container">
                <div class=""></div>
            </div>
        </section> --}}
    </div>

    <!-- Javascript -->
    <script src="{{ url('storage/app/public/new_admin/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/sweetalert/sweetalert2.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/slick/slick.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/custom.js') }}"></script>
</body>

</html>