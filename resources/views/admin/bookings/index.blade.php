@extends('admin.layout.default')
@section('title')
    Bookings List
@endsection
@section('contents')
    <!-- Title -->
    <div class="d-flex align-items-center justify-content-between mb-5">
        <h1 class="h2 mb-0">Bookings</h1>
        @if (Auth::user()->type == 2)
            <a href="{{ URL::to('admin/bookings/add') }}" class="btn btn-primary">Add</a>
        @endif

    </div>
    @if (count($bookings) != 0)
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <input class="form-control list-search mw-300px float-end mb-5" type="search" placeholder="Search">
                    <table class="table table-nowrap mb-0" data-list='{"valueNames": ["id", "name", "manager", "status"]}'>
                        <thead class="thead-light">
                            <tr>
                                @if (Auth::user()->type == 1)
                                    <th scope="col">
                                        <a href="javascript: void(0);" class="text-muted list-sort" data-sort="id">ID</a>
                                    </th>
                                    <th scope="col">
                                        <a href="javascript: void(0);" class="text-muted list-sort"
                                            data-sort="vendor_id">Vendor Name</a>
                                    </th>
                                @endif
                                <th>
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="dome_name">Dome Name</a>
                                </th>
                                <th>
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="dome_price">Dome Price</a>
                                </th>
                                <th scope="col">
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="sports">Sports</a>
                                </th>
                                <th scope="col">
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="start_time">Start Time</a>
                                </th>
                                <th scope="col">
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="end_time">End Time</a>
                                </th>
                                @if (Auth::user()->type == 2)
                                    <th scope="col" class="text-center">
                                        <a href="javascript: void(0);" class="text-muted list-sort" data-sort="action">Action</a>
                                    </th>
                                @endif
                            </tr>
                        </thead>

                        <tbody class="list">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="row justify-content-center">
            <img class="img-fluid w-50" src="{{ Helper::image_path('no_data.svg') }}">
        </div>
    @endif
@endsection

@section('scripts')
    <script>
        // Dome Delete
        function dome_delete(id, status, url) {
            "use strict";
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: "btn btn-success mx-2",
                    cancelButton: "btn btn-danger mx-2",
                },
                buttonsStyling: false,
            });

            swalWithBootstrapButtons
                .fire({
                    title: "Are You Sure?",
                    icon: "warning",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    showCancelButton: true,
                    reverseButtons: true,
                })
                .then((result) => {
                    $("#preloader , #status").show();
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "get",
                            url: url,
                            data: {
                                id: id,
                                status: status,
                            },
                            dataType: "json",
                            success: function(response) {
                                if (response == 1) {
                                    $("#preloader , #status").hide();
                                    toastr.success("Success");
                                    location.reload();
                                } else {
                                    $("#preloader , #status").hide();
                                    Swal.fire({
                                        icon: "error",
                                        title: "Oops...",
                                        text: wrong,
                                    });
                                }
                            },
                            failure: function(response) {
                                $("#preloader , #status").hide();
                                Swal.fire({
                                    icon: "error",
                                    title: "Oops...",
                                    text: wrong,
                                });
                            },
                        });
                    }
                    $("#preloader , #status").hide();
                });
        }
    </script>
@endsection
