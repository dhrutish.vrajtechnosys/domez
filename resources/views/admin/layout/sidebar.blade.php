<!-- Sidebar -->
<nav id="mainNavbar" class="navbar navbar-vertical navbar-expand-lg scrollbar bg-dark navbar-dark">
    <div class="container-fluid">
        <!-- Brand -->
        <a class="navbar-brand text-white text-center fw-semibold fs-10" href="{{ URL::to('admin/dashboard') }}">
            {{-- <img src="https://d33wubrfki0l68.cloudfront.net/33193ecc0db7caa7d7efee3dca8af1b145fa2135/16978/assets/images/logo-small.svg"
                class="navbar-brand-img logo-light logo-small" alt="..." width="19" height="25">
            <img src="https://d33wubrfki0l68.cloudfront.net/ba6b91b7d508187d153e48318c22d0773a9cedfc/36afa/assets/images/logo.svg"
                class="navbar-brand-img logo-light logo-large" alt="..." width="125" height="25">

            <img src="https://d33wubrfki0l68.cloudfront.net/8b6c92837e3b7aa8c9017d33298ead6b9b859d79/fa79b/assets/images/logo-dark-small.svg"
                class="navbar-brand-img logo-dark logo-small" alt="..." width="19" height="25">
            <img src="https://d33wubrfki0l68.cloudfront.net/55307694d1a6b107d2d87c838a1aaede85cd3d84/66f18/assets/images/logo-dark.svg"
                class="navbar-brand-img logo-dark logo-large" alt="..." width="125" height="25"> --}}Admin
        </a>
        <!-- Navbar toggler -->
        <a href="javascript: void(0);" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#sidenavCollapse"
            aria-controls="sidenavCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </a>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenavCollapse">
            <!-- Navigation -->
            <ul class="navbar-nav mb-lg-7">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('admin/dashboard') ? 'active' : '' }}"
                        href="{{ URL::to('admin/dashboard') }}">
                        <i class="fa-duotone fa-house nav-link-icon"></i><span>Dashboards</span>
                    </a>
                </li>

                @if (Auth::user()->type == 1)
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/vendors*') ? 'active' : '' }}"
                            href="{{ URL::to('admin/vendors') }}">
                            <i class="fa-duotone fa-user nav-link-icon"></i><span>Vendor</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/users*') ? 'active' : '' }}"
                            href="{{ URL::to('admin/users') }}">
                            <i class="fa-duotone fa-users nav-link-icon"></i><span>User</span>
                        </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link {{ request()->is('admin/cms*') ? 'active' : '' }}" href="#cmscollapse"
                            data-bs-toggle="collapse" role="button"
                            aria-expanded="{{ request()->is('admin/cms*') ? 'true' : 'false' }}"
                            aria-controls="cmscollapse">
                            <i class="fa-duotone fa-file-pen nav-link-icon"></i><span>CMS Pages</span>
                        </a>
                        <div class="collapse {{ request()->is('admin/cms*') ? 'show' : '' }}" id="cmscollapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/cms/privacy-policy') }}"
                                        class="nav-link {{ request()->is('admin/cms/privacy-policy') ? 'active' : '' }}">Privacy
                                        Policy</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/cms/terms-condition') }}"
                                        class="nav-link {{ request()->is('admin/cms/terms-condition') ? 'active' : '' }}">Terms
                                        & Conditions</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/cms/refund-policy') }}"
                                        class="nav-link {{ request()->is('admin/cms/refund-policy') ? 'active' : '' }}">Refund
                                        Policy</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link {{ request()->is('admin/payment-gateway*') ? 'active' : '' }}"
                            href="#paymentcollapse" data-bs-toggle="collapse" role="button"
                            aria-expanded="{{ request()->is('admin/payment-gateway*') ? 'true' : 'false' }}"
                            aria-controls="paymentcollapse">
                            <i class="fa-duotone fa-money-bill nav-link-icon"></i><span>Payment Gateway</span>
                        </a>
                        <div class="collapse {{ request()->is('admin/payment-gateway*') ? 'show' : '' }}"
                            id="paymentcollapse">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/payment-gateway/stripe') }}"
                                        class="nav-link {{ request()->is('admin/payment-gateway/stripe') ? 'active' : '' }}">Stripe</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/sports*') ? 'active' : '' }}" href="{{ URL::to('admin/sports') }}">
                            <i class="fa-duotone fa-list-tree nav-link-icon"></i><span>Sports</span>
                        </a>
                    </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('admin/transactions*') ? 'active' : '' }}" href="{{ URL::to('admin/transactions') }}">
                        <i class="fa-duotone fa-file-invoice nav-link-icon"></i><span>Transaction</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('admin/domes*') ? 'active' : '' }}" href="{{ URL::to('admin/domes') }}">
                        <i class="fa-duotone fa-house-tree nav-link-icon"></i><span>Domes</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('admin/field*') ? 'active' : '' }}" href="{{ URL::to('admin/field') }}">
                        <i class="fa-duotone fa-list-timeline nav-link-icon"></i><span>Fields</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('admin/bookings*') ? 'active' : '' }}" href="{{ URL::to('admin/bookings') }}">
                        <i class="fa-duotone fa-calendar-days nav-link-icon"></i><span>Bookings</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('admin/settings*') ? 'active' : '' }}" href="{{ URL::to('admin/settings') }}">
                        <i class="fa-duotone fa-gears nav-link-icon"></i><span>Settings</span>
                    </a>
                </li>
            </ul>
            <!-- End of Navigation -->
        </div>
        <!-- End of Collapse -->
    </div>
</nav>
