<!DOCTYPE html>
<html lang="en" data-theme="light" data-sidebar-behaviour="fixed" data-navigation-color="inverted" data-is-fluid="true">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ Helper::image_path('favicon.png') }}" sizes="any">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/fontawesome/all.min.css') }}">
    <!-- Sweetalert CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/sweetalert/sweetalert2.min.css') }}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/theme.bundle.css') }}" id="stylesheetLTR">
    <!-- Toastr CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/toastr/toastr.min.css') }}">
    @yield('styles')
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/custom.css') }}">

    <!-- Page Title -->
    <title>@yield('title') | Domez</title>
</head>

<body>
    <!-- PreLoader -->
    <div id="preloader">
        <div id="status">
            <img src="{{Helper::image_path('preloader.gif')}}" width="150" height="150" alt="Prealoader">
        </div>
    </div>
    @include('admin.layout.sidebar')
    <!-- MAIN CONTENT -->
    <main>

        @include('admin.layout.header')
        <div class="container-fluid">
            @yield('contents')
        </div>
        @include('admin.layout.footer')

    </main>

    <!-- JAVASCRIPT-->
    <script src="{{ url('storage/app/public/admin/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/sweetalert/sweetalert2.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/theme.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/chart.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/list.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/custom.js') }}"></script>
    @yield('scripts')

    <script>
        toastr.options = {
            "closeButton": true
        }
        @if (Session::has('success'))
            toastr.success("{{ session('success') }}");
        @endif

        @if (Session::has('error'))
            toastr.error("{{ session('error') }}");
        @endif

        @if (Session::has('info'))
            toastr.info("{{ session('info') }}");
        @endif

        @if (Session::has('warning'))
            toastr.warning("{{ session('warning') }}");
        @endif
    </script>

</body>

</html>
