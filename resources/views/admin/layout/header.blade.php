<!-- HEADER -->
<header class="container-fluid d-flex py-6 mb-4">
    <!-- Search -->
    <form class="d-none d-md-inline-block me-auto">
        <div class="input-group input-group-merge">

            <!-- Input -->
            <input type="text" class="form-control bg-light-green border-light-green w-250px" id="search_bar" placeholder="Search..."
                aria-label="Search for any term">

            <!-- Button -->
            <span class="input-group-text bg-light-green border-light-green p-0">

                <!-- Button -->
                <button class="btn btn-primary rounded-2 w-30px h-30px p-0 mx-1" type="button"
                    aria-label="Search button"><i class="fa-regular fa-magnifying-glass"></i></button>
            </span>
        </div>
    </form>
    <!-- Top buttons -->
    <div class="d-flex align-items-center ms-auto me-n1 me-lg-n2">

        <!-- Button -->
        <a class="d-flex align-items-center justify-content-center bg-white rounded-circle shadow-sm mx-1 mx-lg-2 w-40px h-40px position-relative link-secondary"
            data-bs-toggle="offcanvas" href="#offcanvasNotifications" role="button"
            aria-controls="offcanvasNotifications">
            <i class="fa-regular fa-bell fs-3"></i>
            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill text-bg-danger">
                20+<span class="visually-hidden">unread messages</span>
            </span>
        </a>

        <!-- Notifications offcanvas -->
        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNotifications"
            aria-labelledby="offcanvasNotificationsLabel">
            <div class="offcanvas-header px-5">
                <h3 class="offcanvas-title" id="offcanvasNotificationsLabel">Notifications</h3>

                <div class="d-flex align-items-start">
                    <div class="dropdown">
                        <a href="javascript: void(0);" class="dropdown-toggle no-arrow w-20px h-20px me-2 text-body"
                            role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="16" width="16">
                                <g>
                                    <circle cx="3.25" cy="12" r="3.25" style="fill: currentColor" />
                                    <circle cx="12" cy="12" r="3.25" style="fill: currentColor" />
                                    <circle cx="20.75" cy="12" r="3.25" style="fill: currentColor" />
                                </g>
                            </svg>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="dropdown-item" href="javascript: void(0);">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                        class="me-2 text-secondary" height="14" width="14">
                                        <g>
                                            <path
                                                d="M23.22,2.06a1.49,1.49,0,0,0-2,.59l-8.5,15.43L6.46,11.29a1.5,1.5,0,1,0-2.21,2l7.64,8.34a1.52,1.52,0,0,0,2.42-.29L23.81,4.1A1.5,1.5,0,0,0,23.22,2.06Z"
                                                style="fill: currentColor" />
                                            <path
                                                d="M2.61,14.63a1.5,1.5,0,0,0-2.22,2l4.59,5a1.52,1.52,0,0,0,2.11.09,1.49,1.49,0,0,0,.1-2.12Z"
                                                style="fill: currentColor" />
                                            <path
                                                d="M10.3,13a1.41,1.41,0,0,0,2-.54L16.89,4.1a1.5,1.5,0,1,0-2.62-1.45L9.68,11A1.41,1.41,0,0,0,10.3,13Z"
                                                style="fill: currentColor" />
                                        </g>
                                    </svg>
                                    Mark as all read
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="javascript: void(0);">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                        class="me-2 text-secondary" height="14" width="14">
                                        <g>
                                            <path
                                                d="M21.5,2.5H2.5a2,2,0,0,0-2,2v3a1,1,0,0,0,1,1h21a1,1,0,0,0,1-1v-3A2,2,0,0,0,21.5,2.5Z"
                                                style="fill: currentColor" />
                                            <path
                                                d="M21.5,10H2.5a1,1,0,0,0-1,1v8.5a2,2,0,0,0,2,2h17a2,2,0,0,0,2-2V11A1,1,0,0,0,21.5,10Zm-6.25,3.5A1.25,1.25,0,0,1,14,14.75H10a1.25,1.25,0,0,1,0-2.5h4A1.25,1.25,0,0,1,15.25,13.5Z"
                                                style="fill: currentColor" />
                                        </g>
                                    </svg>
                                    Archive all
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="javascript: void(0);">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                        class="me-2 text-secondary" height="14" width="14">
                                        <g>
                                            <path
                                                d="M21,19.5a1,1,0,0,0,0-2A1.5,1.5,0,0,1,19.5,16V11.14a8.65,8.65,0,0,0-.4-2.62l-11,11Z"
                                                style="fill: currentColor" />
                                            <path
                                                d="M14.24,21H9.76a.25.25,0,0,0-.24.22,2.64,2.64,0,0,0,0,.28,2.5,2.5,0,0,0,5,0,2.64,2.64,0,0,0,0-.28A.25.25,0,0,0,14.24,21Z"
                                                style="fill: currentColor" />
                                            <path
                                                d="M1,24a1,1,0,0,0,.71-.28l22-22a1,1,0,0,0,0-1.42,1,1,0,0,0-1.42,0l-5,5A7.31,7.31,0,0,0,13,3.07V1a1,1,0,0,0-2,0V3.07a8,8,0,0,0-6.5,8.07V16A1.5,1.5,0,0,1,3,17.5a1,1,0,0,0,0,2h.09L.29,22.29a1,1,0,0,0,0,1.42A1,1,0,0,0,1,24Z"
                                                style="fill: currentColor" />
                                        </g>
                                    </svg>
                                    Disable notifications
                                </a>
                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li>
                                <a class="dropdown-item" href="javascript: void(0);">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                        class="me-2 text-secondary" height="14" width="14">
                                        <g>
                                            <rect x="4.25" y="4.5" width="5.75" height="7.25"
                                                rx="1.25" style="fill: currentColor" />
                                            <path
                                                d="M24,10a3,3,0,0,0-3-3H19V2.5a2,2,0,0,0-2-2H2a2,2,0,0,0-2,2V20a3.5,3.5,0,0,0,3.5,3.5h17A3.5,3.5,0,0,0,24,20ZM3.5,21.5A1.5,1.5,0,0,1,2,20V3a.5.5,0,0,1,.5-.5h14A.5.5,0,0,1,17,3V20a3.51,3.51,0,0,0,.11.87.5.5,0,0,1-.09.44.49.49,0,0,1-.39.19ZM22,20a1.5,1.5,0,0,1-3,0V9.5a.5.5,0,0,1,.5-.5H21a1,1,0,0,1,1,1Z"
                                                style="fill: currentColor" />
                                            <rect x="12" y="6.05" width="3.5" height="2"
                                                rx="0.75" style="fill: currentColor" />
                                            <rect x="12" y="10.05" width="3.5" height="2"
                                                rx="0.75" style="fill: currentColor" />
                                            <rect x="4" y="14.05" width="11.5" height="2"
                                                rx="0.75" style="fill: currentColor" />
                                            <rect x="4" y="18.05" width="9" height="2"
                                                rx="0.75" style="fill: currentColor" />
                                        </g>
                                    </svg>
                                    What's new?
                                </a>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas"
                        aria-label="Close"></button>
                </div>
            </div>

            <div class="offcanvas-body p-0">
                <div class="list-group list-group-flush">
                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="{{ Helper::image_path(Auth::user()->image) }}" alt="..."
                                    class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Daniel</h5>
                                    <small class="text-muted">10 minutes ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">RE: Email pre-population from external source</p>
                                    <small class="text-secondary">Not sure if we'll need any further
                                        instruction on how to utilise the encoded ID in links from the new email
                                        broadcast tool.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <span class="avatar-title text-bg-info-soft">M</span>
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Mochahost.com</h5>
                                    <small class="text-muted">14 minutes ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Customer invoice</p>
                                    <small class="text-secondary">This is a notice that an invoice has been
                                        generated on 05/14/2022.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/f3d8c9fbcd994759c966476a8349d5d053e38964/e7323/assets/images/profiles/profile-26.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Harry</h5>
                                    <small class="text-muted">32 minutes ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Farewell card</p>
                                    <small class="text-secondary">Hi everyone, thanks to all who have already
                                        signed and contributed to Ellie's leaving card.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/0b34af989cce5e54297f16547b3eff1ace44dad5/eb8ea/assets/images/profiles/profile-20.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Gavin</h5>
                                    <small class="text-muted">55 minutes ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Weekly cath up</p>
                                    <small class="text-secondary">Let's see how your emails performed in the
                                        past week.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/b12e43e592a36b25ced24c52bc8ae78595f1f2c6/2ceab/assets/images/profiles/profile-24.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Pamela - HR</h5>
                                    <small class="text-muted">1 hour ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">New starter</p>
                                    <small class="text-secondary">I wanted to introduce Alan to you all, who
                                        starts today in the Operations Team as our new Global Payroll & Benefits
                                        Manager.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/9f5880fc99a40d5021e5a133fe72f232e3883d3a/c965d/assets/images/profiles/profile-13.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">James</h5>
                                    <small class="text-muted">2 hours ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Looking for newsletter analyst</p>
                                    <small class="text-secondary">Good morning Brian, I hope you can help with
                                        the following. I am looking for somebody who can help us create stronger
                                        newsletters.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <span class="avatar-title text-bg-primary-soft">S</span>
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">service.paypal.com</h5>
                                    <small class="text-muted">3 hours ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">You have a Payout!</p>
                                    <small class="text-secondary">Please note that it may take a little while
                                        for this payment to appear in the Activity section of your
                                        account.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <span class="avatar-title text-bg-primary-soft">C</span>
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">CookieYes</h5>
                                    <small class="text-muted">5 hours ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Welcome to CookieYes!</p>
                                    <small class="text-secondary">Welcome to CookieYes! Thank you for creating
                                        an account with us.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/102e41d9e1988e0849ecfe402b1d46f4efd3574b/8dc2e/assets/images/profiles/profile-12.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Andrew</h5>
                                    <small class="text-muted">6 hours ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Congratulations! - and thank you</p>
                                    <small class="text-secondary">Thank you so much for continuing to leave no
                                        stone unturned in pursuit of new milestones of success.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/ea01948f5a48922378b407c27d2b4e5809ed4949/35ecd/assets/images/profiles/profile-11.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Helen</h5>
                                    <small class="text-muted">9 hours ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Bank Holidays season starts tomorrow</p>
                                    <small class="text-secondary">Our office will be closed on these days, as
                                        it will also be on Friday 6 May.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/eec1f115f0af81936bbe3a4f4a4d043cd3c0e7e4/34439/assets/images/profiles/profile-09.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Tiffany</h5>
                                    <small class="text-muted">1 day ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">External meetings and events</p>
                                    <small class="text-secondary">We have updated our external meeting and
                                        events protocol. Please have a look at the office board.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <span class="avatar-title text-bg-danger-soft">II</span>
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Ionos Info</h5>
                                    <small class="text-muted">2 days ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Recommend us to earn attractive commissions</p>
                                    <small class="text-secondary">Happy with your product or service? Sign up
                                        for the IONOS Referral Program to recommend us to your business
                                        partners, friends or family. We'll reward you with attractive
                                        commissions when they place an order.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/102e41d9e1988e0849ecfe402b1d46f4efd3574b/8dc2e/assets/images/profiles/profile-12.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Edward</h5>
                                    <small class="text-muted">3 days ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Website change request</p>
                                    <small class="text-secondary">Please add video overlay option to microsite
                                        header image</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <span class="avatar-title text-bg-primary">BT</span>
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Bootstrap Themes</h5>
                                    <small class="text-muted">3 days ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">[Bootstrap Themes] New order (123456)!</p>
                                    <small class="text-secondary">You've received the following order from
                                        alansmith</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/e83422b2242219796524c41b945eb0a4b4b75dfd/caa0b/assets/images/profiles/profile-08.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Greg</h5>
                                    <small class="text-muted">4 days ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Greg Smith (Jira) 2</p>
                                    <small class="text-secondary">[JIRA] (WEB-1022) Add Full Width Video
                                        Content Block to microsites</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/790b7dd581a3ac4fd0410afad0fb12c6e93c9e7a/b0657/assets/images/profiles/profile-07.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Michael</h5>
                                    <small class="text-muted">5 days ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">Hard drive limit</p>
                                    <small class="text-secondary">Your hard drive is close to its storage cap.
                                        Once exceeded, you can't add new items or sync changes.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <span class="avatar-title text-bg-info">RC</span>
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Rave Coffee</h5>
                                    <small class="text-muted">6 days ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">It's Double Points - ⏰ 24 hours only</p>
                                    <small class="text-secondary">Login to your Rave account to place your
                                        order and you will automatically earn double points on every $
                                        spent.</small>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="javascript: void(0);" class="list-group-item list-group-item-action">
                        <div class="d-flex">
                            <div class="avatar avatar-circle avatar-xs me-2">
                                <img src="https://d33wubrfki0l68.cloudfront.net/4b8c918c73e2c72876e4bd4ba8c89401bae69d14/5923c/assets/images/profiles/profile-03.jpeg"
                                    alt="..." class="avatar-img" width="30" height="30">
                            </div>

                            <div class="d-flex flex-column flex-grow-1">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">John</h5>
                                    <small class="text-muted">7 days ago</small>
                                </div>

                                <div class="d-flex flex-column">
                                    <p class="mb-1">John Po (Jira)</p>
                                    <small class="text-secondary">Improving slide arrows and indicators on
                                        gift impact, testimonial and victories module</small>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- Separator -->
        <div class="vr bg-gray-700 mx-2 mx-lg-3"></div>

        <!-- Dropdown -->
        <div class="dropdown">
            <a href="javascript: void(0);"
                class="dropdown-toggle no-arrow d-flex align-items-center justify-content-center bg-white rounded-circle shadow-sm mx-1 mx-lg-2 w-40px h-40px"
                role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true"
                aria-expanded="false" data-bs-offset="0,10">
                <div class="avatar avatar-circle avatar-sm avatar-online">
                    <img src="{{ Helper::image_path(Auth::user()->image) }}" alt="..." class="avatar-img"
                        width="40" height="40">
                </div>
            </a>

            <div class="dropdown-menu">
                <div class="dropdown-item-text">
                    <div class="d-flex align-items-center">
                        <div class="avatar avatar-sm avatar-circle">
                            <img src="{{ Helper::image_path(Auth::user()->image) }}" alt="..."
                                class="avatar-img" width="40" height="40">
                        </div>
                        <div class="flex-grow-1 ms-3">
                            <h4 class="mb-0">{{ Auth::user()->name }}</h4>
                            <p class="card-text">{{ Auth::user()->email }}</p>
                        </div>
                    </div>
                </div>

                <hr class="dropdown-divider">
                <a class="dropdown-item" href="javascript: void(0);">Profile & account</a>
                <a class="dropdown-item" href="javascript: void(0);">Settings</a>

                <hr class="dropdown-divider">

                <a class="dropdown-item" onclick="logout('{{ URL::to('logout') }}')">Sign out</a>
            </div>
        </div>
    </div>
</header>
