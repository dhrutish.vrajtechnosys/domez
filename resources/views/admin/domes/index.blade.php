@extends('admin.layout.default')
@section('title')
    Domes List
@endsection
@section('contents')
    <!-- Title -->
    <div class="d-flex align-items-center justify-content-between mb-5">
        <h1 class="h2 mb-0">Domes</h1>
        @if (Auth::user()->type == 2)
            <a href="{{ URL::to('admin/domes/add') }}" class="btn btn-primary">Add</a>
        @endif

    </div>
    @if (count($domes) != 0)
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <input class="form-control list-search mw-300px float-end mb-5" type="search" placeholder="Search">
                    <table class="table table-nowrap mb-0" data-list='{"valueNames": ["id", "name", "manager", "status"]}'>
                        <thead class="thead-light">
                            <tr>
                                @if (Auth::user()->type == 1)
                                    <th scope="col">
                                        <a href="javascript: void(0);" class="text-muted list-sort" data-sort="id">ID</a>
                                    </th>
                                    <th scope="col">
                                        <a href="javascript: void(0);" class="text-muted list-sort"
                                            data-sort="vendor_id">Vendor Name</a>
                                    </th>
                                @endif
                                <th>
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="dome_name">Dome Name</a>
                                </th>
                                <th>
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="dome_price">Dome Price</a>
                                </th>
                                <th scope="col">
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="sports">Sports</a>
                                </th>
                                <th scope="col">
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="start_time">Start Time</a>
                                </th>
                                <th scope="col">
                                    <a href="javascript: void(0);" class="text-muted list-sort" data-sort="end_time">End Time</a>
                                </th>
                                @if (Auth::user()->type == 2)
                                    <th scope="col" class="text-center">
                                        <a href="javascript: void(0);" class="text-muted list-sort" data-sort="action">Action</a>
                                    </th>
                                @endif
                            </tr>
                        </thead>

                        <tbody class="list">
                            @php $i =1 @endphp
                            @foreach ($domes as $dome)
                                @php
                                    $cat_id = explode('|', $dome->sport_id);
                                @endphp
                                <tr>
                                    @if (Auth::user()->type == 1)
                                        <th scope="row">{{ $i++ }}</th>
                                        <td class="vendor_id">{{ $dome['vendor']->name }}</td>
                                    @endif
                                    <td class="dome_name">{{ $dome->name }}</td>
                                    <td class="price">{{ $dome->price }}</td>
                                    <td class="category">
                                        @foreach ($categories as $i => $category)
                                            @if (in_array($category->id, $cat_id))
                                                {{ $category->name }} {{ $i + 1 != count($cat_id) ? '|' : '' }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="start_time">{{ $dome->start_time }}</td>
                                    <td class="end_time">{{ $dome->end_time }}</td>
                                    @if (Auth::user()->type == 2)
                                        <td class="action text-center">
                                            <a href="{{ URL::to('admin/domes/edit-') . $dome->id }}"
                                                class="text-secondary me-2 fs-3" data-bs-toggle="tooltip"
                                                data-bs-placement="bottom" data-bs-title="Edit"><i
                                                    class="fa-duotone fa-pen-to-square"></i></a>
                                            <a onclick="dome_delete('{{ $dome->id }}','{{ $dome->is_deleted == 2 ? 1 : 2 }}','{{ URL::to('admin/domes/delete') }}')"
                                                class="text-danger me-2 fs-3" data-bs-toggle="tooltip"
                                                data-bs-placement="bottom" data-bs-title="Delete"><i
                                                    class="fa-duotone fa-trash-can"></i></a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="row justify-content-center">
            <img class="img-fluid w-50" src="{{ Helper::image_path('no_data.svg') }}">
        </div>
    @endif
@endsection

@section('scripts')
    <script>
        // Dome Delete
        function dome_delete(id, status, url) {
            "use strict";
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: "btn btn-success mx-2",
                    cancelButton: "btn btn-danger mx-2",
                },
                buttonsStyling: false,
            });

            swalWithBootstrapButtons
                .fire({
                    title: "Are You Sure?",
                    icon: "warning",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    showCancelButton: true,
                    reverseButtons: true,
                })
                .then((result) => {
                    $("#preloader , #status").show();
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "get",
                            url: url,
                            data: {
                                id: id,
                                status: status,
                            },
                            dataType: "json",
                            success: function(response) {
                                if (response == 1) {
                                    $("#preloader , #status").hide();
                                    toastr.success("Success");
                                    location.reload();
                                } else {
                                    $("#preloader , #status").hide();
                                    Swal.fire({
                                        icon: "error",
                                        title: "Oops...",
                                        text: wrong,
                                    });
                                }
                            },
                            failure: function(response) {
                                $("#preloader , #status").hide();
                                Swal.fire({
                                    icon: "error",
                                    title: "Oops...",
                                    text: wrong,
                                });
                            },
                        });
                    }
                    $("#preloader , #status").hide();
                });
        }
    </script>
@endsection
