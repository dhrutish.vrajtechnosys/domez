@extends('admin.layout.default')
@section('title') Edit Vendor @endsection
@section('contents')
    <h1 class="h2">Edit Vendor</h1>

    <div class="row">
        <div class="col-md-8">
            <form class="card" action="{{URL::to('admin/vendors/update-').$userdata->id}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="mb-4">
                        <label class="form-label" for="name">Name</label>
                        <input type="text" id="name" name="name" value="{{$userdata->name}}" class="form-control" placeholder="Please Enter Name">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>    
                    <div class="mb-4">
                        <label class="form-label" for="email">Email</label>
                        <input type="email" id="email" name="email" value="{{$userdata->email}}" class="form-control" placeholder="Please Enter Email">
                        @error('email') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="mb-4">
                        <label class="form-label" for="profile">Profile Image<span class="form-label-secondary px-2">(Optional)</span></label>
                        <input type="file" name="profile" id="profile" class="form-control">
                        @error('profile') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="avatar avatar-xxl avatar-circle mb-6">
                        <img src="{{Helper::image_path($userdata->image)}}" alt="..." class="avatar-img">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
