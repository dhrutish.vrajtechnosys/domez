@extends('admin.layout.default')
@section('title')
    Vendors
@endsection
@section('contents')
    <!-- Title -->
    <div class="d-flex align-items-center justify-content-between mb-5">
        <h1 class="h2 mb-0">Vendor List</h1>
        <a href="{{ URL::to('admin/vendors/add') }}" class="btn btn-primary">Add</a>
    </div>


    @if (count($users) != 0)
        <div class="row row-cols-xl-4 row-cols-lg-3 row-cols-md-2 row-cols-auto search_row">
            @foreach ($users as $user)
                <div class="col" id="search_vendor">
                    <div class="card">
                        <div class="d-grid justify-items-center">
                            <div class="avatar avatar-xxl avatar-circle mt-5">
                                <img src="{{ Helper::image_path($user->image) }}" alt="..." class="avatar-img">
                            </div>
                        </div>
                        <div class="img-overlay">
                            <a class="dropdown" href="javascript: void(0);" role="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="fa-solid fa-ellipsis-vertical fs-3 text-muted"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::to('admin/vendors/edit-') . $user->id }}"
                                        class="dropdown-item">Edit</a></li>
                                <li>
                                    <a onclick="vendor_delete('{{ $user->id }}','{{ $user->is_deleted == 2 ? 1 : 2 }}','{{ URL::to('admin/vendors/delete') }}')"
                                        class="dropdown-item {{ $user->is_deleted == 2 ? 'text-danger' : 'text-success' }}">
                                        {{ $user->is_deleted == 2 ? 'Delete' : 'Recover' }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <h3 class="card-title text-center" id="vendor_name">{{ $user->name }}</h3>
                            <div class="row justify-content-between mb-5">
                                <div class="col-6 text-center">
                                    <h3 class="mb-0">4</h3>
                                    <p class="mb-0 fw-light">Domes</p>
                                </div>
                                <div class="col-6 text-center">
                                    <h3 class="mb-0">15</h3>
                                    <p class="mb-0 fw-light">Bookings</p>
                                </div>
                            </div>
                            <a onclick="change_status('{{ $user->id }}','{{ $user->is_available == 1 ? 2 : 1 }}','{{ URL::to('admin/vendors/change_status') }}')"
                                class="btn btn-success {{ $user->is_available == 1 ? 'btn-success' : 'btn-danger' }} d-grid">{{ $user->is_available == 1 ? 'Active' : 'Inactive' }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <div class="row justify-content-center">
            <img class="img-fluid w-50" src="{{ Helper::image_path('no_data.svg') }}">
        </div>
    @endif
@endsection

@section('scripts')
    <script>
        // live search
        $(document).ready(function() {
            "use strict";
            $("#search_bar").keyup(function() {
                var value = $(this).val().toLowerCase();
                $(".search_row #search_vendor").filter(function() {
                    $(this).toggle($(this).find('#vendor_name').text().toLowerCase().indexOf(
                        value) > -1)
                });
            });
        });

        // Vendor Delete
        function vendor_delete(id, status, url) {
            "use strict";
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: "btn btn-success mx-2",
                    cancelButton: "btn btn-danger mx-2",
                },
                buttonsStyling: false,
            });

            swalWithBootstrapButtons
                .fire({
                    title: "Are You Sure?",
                    icon: "warning",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    showCancelButton: true,
                    reverseButtons: true,
                })
                .then((result) => {
                    $("#preloader , #status").show();
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "get",
                            url: url,
                            data: {
                                id: id,
                                status: status,
                            },
                            dataType: "json",
                            success: function(response) {
                                if (response == 1) {
                                    $("#preloader , #status").hide();
                                    toastr.success("Success");
                                    location.reload();
                                } else {
                                    $("#preloader , #status").hide();
                                    Swal.fire({
                                        icon: "error",
                                        title: "Oops...",
                                        text: wrong,
                                    });
                                }
                            },
                            failure: function(response) {
                                $("#preloader , #status").hide();
                                Swal.fire({
                                    icon: "error",
                                    title: "Oops...",
                                    text: wrong,
                                });
                            },
                        });
                    }
                    $("#preloader , #status").hide();
                });
        }
    </script>
@endsection
