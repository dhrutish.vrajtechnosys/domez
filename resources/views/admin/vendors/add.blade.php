@extends('admin.layout.default')
@section('title') Add Vendor @endsection
@section('contents')
    <h1 class="h2">Add Vendor</h1>

    <div class="row">
        <div class="col-md-8">
            <form class="card" action="{{URL::to('admin/vendors/store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="mb-4">
                        <label class="form-label" for="name">Name</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Please Enter Name">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>    
                    <div class="mb-4">
                        <label class="form-label" for="email">Email</label>
                        <input type="email" id="email" name="email" class="form-control" placeholder="Please Enter Email">
                        @error('email') <span class="text-danger">{{$message}}</span> @enderror
                    </div> 
                    <div class="mb-4">
                        <label class="form-label" for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Please Enter Password">                                
                        @error('password') <span class="text-danger">{{$message}}</span> @enderror
                    </div>    
                    <div class="mb-6">
                        <label class="form-label" for="profile">Profile Image<span class="form-label-secondary">(Optional)</span></label>
                        <input type="file" name="profile" id="profile" class="form-control">
                        @error('profile') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
