<!DOCTYPE html>
<html lang="en" data-theme="light" data-sidebar-behaviour="fixed" data-navigation-color="inverted" data-is-fluid="true">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ url('storage/app/public/admin/images/favicon/favicon.ico') }}" sizes="any">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/fontawesome/all.min.css') }}">
    <!-- Sweetalert CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/sweetalert/sweetalert2.min.css') }}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/theme.bundle.css') }}" id="stylesheetLTR">
    <!-- Toastr CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/toastr/toastr.min.css') }}">
    @yield('styles')
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/custom.css') }}">

    <!-- Page Title -->
    <title>Verification | Domez</title>
</head>

<body>
    <!-- MAIN CONTENT -->
    <main class="container">
        <div class="row align-items-center justify-content-around vh-100">
            <div class="col-lg-10">
                <div class="row align-items-center justify-content-between py-6">
                    <div class="col-lg-5 order-lg-last">
    
                        <!-- Image -->
                        <div class="text-center">
                           <img src="{{Helper::image_path('verification.svg')}}" alt="..." class="img-fluid">
                        </div>
                   </div>
    
                    <div class="col-lg-6 py-6">
    
                        <!-- Title -->
                        <h1 class="mb-2">Email Verification</h1>

                        <!-- Heading -->
                        <p class="text-secondary">We sent a verification code to your email. <br class="d-none d-lg-block">Enter the code from the email in the field below</p>
    
                        <!-- Form -->
                        <form action="{{URL::to('verify')}}" method="post">
                            @csrf
                            <!-- Inputs -->
                            <div class="row mx-n3">
                                <div class="col-auto px-3">
                                    <div class="mb-6">
                                        <input type="text" class="form-control" placeholder="Please Enter OTP" name="otp">
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center justify-content-md-between text-center text-md-start">
                                <div class="col-md-auto">    
                                    <!-- Button -->
                                    <button type="submit" class="btn btn-primary mt-3 mb-1">Verify my account</button>
                                </div>                                
                                <div class="col-md-auto">    
                                    <!-- Link -->
                                    <small class="mb-0 text-muted">Haven't received it? <a href="{{URL::to('resend-otp')}}" class="fw-semibold">Resend a new code</a></small>
                                </div>
                            </div> <!-- / .row -->
                        </form>
                    </div>
                </div> <!-- / .row -->
            </div>
        </div> <!-- / .row -->
    </main>

    <!-- JAVASCRIPT-->
    <script src="{{ url('storage/app/public/admin/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/sweetalert/sweetalert2.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/theme.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/chart.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/list.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/custom.js') }}"></script>
    @yield('scripts')
    <script>
        toastr.options = {
            "closeButton": true
        }
        @if (Session::has('success'))
            toastr.success("{{ session('success') }}");
        @endif

        @if (Session::has('error'))
            toastr.error("{{ session('error') }}");
        @endif

        @if (Session::has('info'))
            toastr.info("{{ session('info') }}");
        @endif

        @if (Session::has('warning'))
            toastr.warning("{{ session('warning') }}");
        @endif
    </script>
</body>

</html>
