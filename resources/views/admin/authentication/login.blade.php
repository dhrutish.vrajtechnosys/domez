<!DOCTYPE html>
<html lang="en" data-theme="light" data-sidebar-behaviour="fixed" data-navigation-color="inverted" data-is-fluid="true">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ url('storage/app/public/admin/images/favicon/favicon.ico') }}" sizes="any">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/fontawesome/all.min.css') }}">
    <!-- Sweetalert CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/sweetalert/sweetalert2.min.css') }}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/theme.bundle.css') }}" id="stylesheetLTR">
    <!-- Toastr CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/toastr/toastr.min.css') }}">
    @yield('styles')
    <link rel="stylesheet" href="{{ url('storage/app/public/admin/css/custom.css') }}">

    <!-- Page Title -->
    <title>Login | Domez</title>
</head>

<body>
    <!-- MAIN CONTENT -->
    <main class="container">
        <div class="row align-items-center justify-content-center vh-100">
            <div class="col-md-7 col-lg-6 d-flex flex-column py-6">
                <!-- Title -->
                <h1 class="mb-2">Sign In</h1>

                <!-- Subtitle -->
                <p class="text-secondary">Enter your email address and password to access admin panel</p>

                <!-- Form -->
                <form action="{{ URL::to('checklogin') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-4">
                                <!-- Label -->
                                <label class="form-label" for="email">Email Address</label>
                                <!-- Input -->
                                <input type="email" id="email" name="email" class="form-control"
                                    placeholder="Your email address">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <!-- Password -->
                            <div class="mb-4">
                                <div class="row">
                                    <div class="col">
                                        <!-- Label -->
                                        <label class="form-label" for="password">Password</label>
                                    </div>
                                    <div class="col-auto">
                                        <!-- Help text -->
                                        <a href="{{ URL::to('forgot_password') }}"
                                            class="form-text small text-muted link-primary">Forgot password</a>
                                    </div>
                                </div> <!-- / .row -->

                                <!-- Input -->
                                <div class="input-group input-group-merge">
                                    <input type="password" id="password" name="password" class="form-control"
                                        autocomplete="off" data-toggle-password-input="" placeholder="Your password">
                                    <button type="button" class="input-group-text px-4 text-secondary link-primary"
                                        data-toggle-password=""></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mt-3">
                        <!-- Button -->
                        <button type="submit" class="btn btn-primary">Sign in</button>
                        <!-- Link -->
                        <small class="mb-0 text-muted">Don't have an account yet?
                            <a href="{{ URL::to('register') }}" class="fw-semibold">Sign up</a>
                        </small>
                    </div>
                </form>
                <div class="or_section">
                    <div class="line ms-4"></div>
                    <p class="mb-0">OR</p>
                    <div class="line me-4"></div>
                </div>
                <div class="row m-3 text-center social_icon">
                    <div class="col">
                        <a href="{{URL::to('auth/apple')}}" class="p-4">
                            <img src="{{Helper::image_path('apple.svg')}}" alt="social-icon">
                        </a>
                        <a href="{{URL::to('auth/google')}}" class="p-4">
                            <img src="{{Helper::image_path('google.svg')}}" alt="social-icon">
                        </a>
                        <a href="{{URL::to('auth/facebook')}}" class="p-4">
                            <img src="{{Helper::image_path('facebook.svg')}}" alt="social-icon">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-5 col-lg-6 px-lg-4 px-xl-6 d-none d-lg-block">

                <!-- Image -->
                <div class="text-center">
                    <img src="{{ Helper::image_path('login.svg') }}" alt="..." class="img-fluid">
                </div>
            </div>
        </div> <!-- / .row -->
    </main>

    <!-- JAVASCRIPT-->
    <script src="{{ url('storage/app/public/admin/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/sweetalert/sweetalert2.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/theme.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/chart.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/list.bundle.js') }}"></script>
    <script src="{{ url('storage/app/public/admin/js/custom.js') }}"></script>
    @yield('scripts')

    <script>
        toastr.options = {
            "closeButton": true
        }
        @if (Session::has('success'))
            toastr.success("{{ session('success') }}");
        @endif

        @if (Session::has('error'))
            toastr.error("{{ session('error') }}");
        @endif

        @if (Session::has('info'))
            toastr.info("{{ session('info') }}");
        @endif

        @if (Session::has('warning'))
            toastr.warning("{{ session('warning') }}");
        @endif
    </script>

</body>

</html>
