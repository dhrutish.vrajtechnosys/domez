@extends('admin.layout.default')
@section('title') Users @endsection
@section('contents')

    @if (count($users) != 0)
        <!-- Title -->
        <h1 class="h2 mb-0">User List</h1>
        <div class="row row-cols-xl-4 row-cols-lg-3 row-cols-md-2 row-cols-auto">
            @foreach ($users as $user)
                <div class="col">
                    <div class="card">
                        <div class="d-grid justify-items-center">
                            <div class="avatar avatar-xxl avatar-circle mt-5">
                                <img src="{{ Helper::image_path($user->image) }}" alt="..." class="avatar-img">
                            </div>
                        </div>
                        <div class="img-overlay">
                            <a class="dropdown" href="javascript: void(0);" role="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="fa-solid fa-ellipsis-vertical fs-3 text-muted"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="dropdown-item">Edit</a></li>
                                <li><a href="#" class="dropdown-item">{{ $user->is_deleted == 2 ? 'Delete' : 'Recover' }}</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <h3 class="card-title text-center">{{ $user->name }}</h3>
                            <div class="row justify-content-between mb-5">
                                <div class="col-6 text-center">
                                    <h3 class="mb-0">4</h3>
                                    <p class="mb-0 fw-light">Domes</p>
                                </div>
                                <div class="col-6 text-center">
                                    <h3 class="mb-0">15</h3>
                                    <p class="mb-0 fw-light">Bookings</p>
                                </div>
                            </div>
                            <a onclick="change_status('{{ $user->id }}','{{ $user->is_available == 1 ? 2 : 1 }}','{{ URL::to('admin/vendors/change_status') }}')"
                                class="btn btn-success {{ $user->is_available == 1 ? 'btn-success' : 'btn-danger' }} d-grid">{{ $user->is_available == 1 ? 'Active' : 'Inactive' }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <div class="row justify-content-center">
            <img class="img-fluid w-50" src="{{ Helper::image_path('no_data.svg') }}">
        </div>
    @endif
@endsection
