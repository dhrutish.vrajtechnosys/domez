<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Page Title -->
    <title>@yield('title') | Domez</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ Helper::image_path('favicon.png') }}" sizes="any">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/bootstrap/bootstrap.min.css') }}">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/fontawesome/all.min.css') }}">
    <!-- Sweetalert CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/sweetalert/sweetalert2.min.css') }}">
    <!-- Toastr CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/toastr/toastr.min.css') }}">
    <!-- Datatables CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/datatable/datatables.min.css') }}">
    @yield('styles')
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/custom.css') }}">
</head>

<body>
    <!-- PreLoader -->
    <div id="preloader">
        <div id="status">
            <img src="{{Helper::image_path('preloader.gif')}}" width="150" height="150" alt="Prealoader">
        </div>
    </div>
    <main>
        <style>
            :root {
                --border-radius: 6px;
                --bs-primary: #468F72;
                --bs-secondary: #57A700;
                --bs-primary-rgb: 70, 143, 114;
                --bs-secondary-rgb: 87, 167, 0;
                --border-default: 1px solid rgba(var(--bs-primary-rgb), 0.25);
            }
        </style>
        <div class="container-fluid px-0">
            @if (!empty(Auth::user()))
                @include('new_admin.layout.header')
            @endif
            <div class="d-flex">
                @if (!empty(Auth::user()))
                    @include('new_admin.layout.sidebar')
                @endif
                <div class="content-wrapper me-3">
                    @yield('contents')
                </div>
            </div>
        </div>
        @if (!empty(Auth::user()))
            @include('new_admin.layout.footer')
        @endif
    </main>



    <!-- Javascript -->
    <script src="{{ url('storage/app/public/new_admin/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/sweetalert/sweetalert2.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/datatable/dataTables.bootstrap5.min.js') }}"></script>
    @yield('scripts')
    <script src="{{ url('storage/app/public/new_admin/js/custom.js') }}"></script>

    <script>
        // Datatables
        $(document).ready(function() {
            $('#datatable').DataTable({
                "paging": true,
                "scrollX": false,
                "scrollY": false,
                buttons: [ 'copy', 'csv', 'excel' ]
            });
        });
        // Toastr
        toastr.options = {
            "closeButton": true
        }
        @if (Session::has('success'))
            toastr.success("{{ session('success') }}");
        @endif

        @if (Session::has('error'))
            toastr.error("{{ session('error') }}");
        @endif

        @if (Session::has('info'))
            toastr.info("{{ session('info') }}");
        @endif

        @if (Session::has('warning'))
            toastr.warning("{{ session('warning') }}");
        @endif
    </script>

</body>

</html>
