<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Page Title -->
    <title>Sign in | Domez</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ Helper::image_path('preloader.gif') }}" sizes="any">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/bootstrap/bootstrap.min.css') }}">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/fontawesome/all.min.css') }}">
    <!-- Sweetalert CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/sweetalert/sweetalert2.min.css') }}">
    <!-- Toastr CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/toastr/toastr.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ url('storage/app/public/new_admin/css/custom.css') }}">
</head>

<body>
    <style>
        :root {
            --border-radius: 6px;
            --bs-primary: #468F72;
            --bs-secondary: #57A700;
            --bs-primary-rgb: 70, 143, 114;
            --bs-secondary-rgb: 87, 167, 0;
            --bs-link-color: var(--bs-primary);
            --bs-link-hover-color: #1E88E5;
            --border-default: 1px solid rgba(var(--bs-primary-rgb), 0.25);
        }

        .btn-primary {
            --bs-btn-hover-bg: #1E88E5;
            --bs-btn-hover-border-color: #1E88E5;
            --bs-btn-focus-shadow-rgb: 49, 132, 253;
            --bs-btn-active-bg: #1E88E5;
            --bs-btn-active-border-color: #1E88E5;
            --bs-btn-active-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
        }

        .btn-secondary {
            --bs-btn-hover-bg: #5E35B1;
            --bs-btn-hover-border-color: #5E35B1;
            --bs-btn-focus-shadow-rgb: 130, 138, 145;
            --bs-btn-active-bg: #5E35B1;
            --bs-btn-active-border-color: #5E35B1;
            --bs-btn-active-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
        }
    </style>
    <!-- PreLoader -->
    <div id="preloader">
        <div id="status">
            <img src="{{ Helper::image_path('preloader.gif') }}" width="150" height="150" alt="Prealoader">
        </div>
    </div>
    <section class="auth-bg">
        <div class="row justify-content-center align-items-center g-0 w-100 h-100vh">
            <div class="col-xl-4 col-lg-6 col-sm-8 px-5">
                <div class="card auth-wrapper box-shadow">
                    <a href="{{ URL::to('/') }}" class="logo">
                        <img src="{{ Helper::image_path('logo_dark.png') }}" alt="" srcset="">
                    </a>
                    <h4 class="text-secondary text-center fw-semibold text-capitalize">{{ trans('labels.hi') }},
                        {{ trans('labels.welcome_back') }}</h4>
                    {{-- <div class="row text-center my-4">
                        <div class="col">
                            <a href="#" class="py-4 px-2 text-decoration-none">
                                <img src="{{ Helper::image_path('apple.svg') }}" alt="social-auth">
                            </a>
                            <a href="#" class="py-4 px-2 text-decoration-none">
                                <img src="{{ Helper::image_path('google.svg') }}" alt="social-auth">
                            </a>
                            <a href="#" class="py-4 px-2 text-decoration-none">
                                <img src="{{ Helper::image_path('facebook.svg') }}" alt="social-auth">
                            </a>
                        </div>
                    </div>
                    <p class="text-muted text-center">Enter your credentials to continue</p>
                    <div class="or_section">
                        <div class="line ms-4"></div>
                        <p class="fw-semibold">OR</p>
                        <div class="line me-4"></div>
                    </div>
                    <p class="text-center fw-semibold">Sign in with Email Address</p> --}}
                    <form action="{{ URL::to('checklogin') }}" method="POST" class="card-body pb-0">
                        @csrf
                        <div class="form-floating">
                            <input type="email" name="email" id="email" class="form-control"
                                placeholder="{{ trans('messages.enter_email_address') }}">
                            <label for="email">{{ trans('labels.email_address') }}</label>
                            @error('email')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-floating">
                            <input type="password" name="password" id="password" class="form-control"
                                placeholder="{{ trans('messages.enter_password') }}">
                            <label for="password">{{ trans('labels.password') }}</label>
                            @error('password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="remember_me" checked>
                                <label class="form-check-label"
                                    for="remember_me">{{ trans('labels.remember_me') }}</label>
                            </div>
                            <a href="{{ URL::to('forgot-password') }}"
                                class="text-secondary float-end text-decoration-none">{{ trans('labels.forgot_password') }}?</a>
                        </div>
                        <button type="submit"
                            class="btn btn-secondary w-100 mt-2">{{ trans('labels.sign_in') }}</button>
                        {{-- <div class="border-bottom my-4"></div>
                        <div class="text-center">
                            <a href="{{URL::to('register')}}" class="text-dark text-decoration-none fs-7 fw-500 text-center">Don't have an account?</a>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Javascript -->
    <script src="{{ url('storage/app/public/new_admin/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/sweetalert/sweetalert2.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/toastr/toastr.min.js') }}"></script>
    <script src="{{ url('storage/app/public/new_admin/js/custom.js') }}"></script>
</body>

</html>
