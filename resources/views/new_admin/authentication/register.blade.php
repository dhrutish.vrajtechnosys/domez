@extends('new_admin.layout.default')
@section('title')
    Sign Up
@endsection

@section('contents')
    <section class="auth-bg">
        <div class="row justify-content-center align-items-center g-0 w-100 h-100vh">
            <div class="col-xl-4 col-lg-6 col-sm-8 px-5">
                <div class="card auth-wrapper">
                    <div class="logo">
                        <img src="{{ Helper::image_path('logo_dark.png') }}" alt="" srcset="">
                    </div>
                    <h4 class="text-secondary text-center fw-semibold text-capitalize mb-4">Sign Up</h4>
                    <p class="text-center fw-semibold">Sign up with Email Address</p>
                    <form action="{{URL::to('store-register')}}" method="POST" class="card-body pb-0">
                        @csrf
                        <div class="form-floating">
                            <input type="name" name="name" id="name" class="form-control" placeholder="Enter Your Full Name">
                            <label for="name">Full Name</label>
                            @error('name') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                        <div class="form-floating">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email Address">
                            <label for="email">Email Address</label>
                            @error('email') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                        <div class="form-floating">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter Password">
                            <label for="password">Password</label>
                            @error('password') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                        <div class="form-floating">
                            <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Enter Confirm Password">
                            <label for="cpassword">Confirm Password</label>
                            @error('cpassword') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="remember_me" checked>
                            <label class="form-check-label" for="remember_me">Remember Me</label>
                        </div>
                        <button type="submit" class="btn btn-secondary w-100 mt-2">Sign Up</button>
                        <div class="border-bottom my-4"></div>
                        <div class="text-center">
                            <a href="{{URL::to('/')}}" class="text-dark text-decoration-none fs-7 fw-500 text-center">Already have an account?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
