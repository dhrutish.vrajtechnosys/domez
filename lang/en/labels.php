<?php
return [
    'domez' => 'Domez',
    'hi' => 'Hi',
    'welcome_back' => 'Welcome Back',
    'email_address' => 'Email Address',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'forgot_password' => 'Forgot Password',
    'sign_in' => 'Sign In',
    'continue' => 'Continue',
    'back_to' => 'Back to',
    'check_email' => 'Check Email Inbox'
];
