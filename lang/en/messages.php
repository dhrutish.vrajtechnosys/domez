<?php
return [
    // Placeholders
    'enter_email_address' => 'Enter Email Address',
    'enter_password' => 'Enter Password',

    // Notes
    'forgot_password_note' => "Enter the email address associated with your account and we'll send you a link to reset your password.",
    'check_mail_note' => "We have sent a password recover instructions to your email."
];
