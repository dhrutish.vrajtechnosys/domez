<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Domes extends Model
{
    use HasFactory;
    public function dome_image()
    {
        return $this->hasOne('App\Models\DomeImages', 'id', 'dome_id')->select('id', DB::raw("CONCAT('" . url('storage/app/public/admin/images/domes') . "/', images) AS image"));
    }
    public function dome_images()
    {
        return $this->hasMany('App\Models\DomeImages', 'id', 'dome_id')->select('id', DB::raw("CONCAT('" . url('storage/app/public/admin/images/domes') . "/', images) AS image"));
    }
}
