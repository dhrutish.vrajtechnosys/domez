<?php

namespace App\Http\Controllers\api;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthenticationController extends Controller
{

    public function sign_in(Request $request)
    {
        if ($request->email == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Email"], 200);
        }
        if ($request->password == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Password"], 200);
        }
        $checkuser = User::where('email', $request->email)->where('type', 3)->first();
        if (!empty($checkuser)) {
            if (Hash::check($request->password, $checkuser->password)) {
                if ($checkuser->is_available == 1) {
                    if ($checkuser->is_verified == 1) {
                        if ($checkuser->is_deleted == 2) {
                            $user = array(
                                'id' => $checkuser->id,
                                'name' => $checkuser->name,
                                'email' => $checkuser->email,
                                'phone' => $checkuser->phone == "" ? "" : $checkuser->phone,
                                'image' => Helper::image_path($checkuser->image),
                            );
                            return response()->json(['status' => 1, 'message' => "Sign In Done Successfully", 'userdata' => $user], 200);
                        } else {
                            return response()->json(['status' => 0, 'message' => "Account Has Been Deleted"], 200);
                        }
                    } else {
                        $otp = rand(1000, 9999);
                        $user = User::where('email', $request->email)->first();
                        $user->otp = $otp;
                        $user->save();
                        $send_mail = Helper::verificationemail($request->email, $checkuser->name, $otp);

                        if ($send_mail == 1) {
                            return response()->json(['status' => 1, 'message' => "OTP Sent Successfully On Email"], 200);
                        } else {
                            return response()->json(['status' => 0, 'message' => "Email Error"], 200);
                        }
                    }
                } else {
                    return response()->json(['status' => 0, 'message' => "Blocked By Admin"], 200);
                }
            } else {
                return response()->json(['status' => 0, 'message' => "Invalid Email/Password"], 200);
            }
        } else {
            return response()->json(['status' => 0, 'message' => "Invalid Email/Password"], 200);
        }
    }
    public function sign_up(Request $request)
    {
        $checkemail = User::where('email', $request->email)->first();

        if ($request->name == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Your Name"], 200);
        }
        if ($request->email == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Email"], 200);
        }
        if ($request->phone == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Phone Number"], 200);
        }
        if (!empty($checkemail)) {
            return response()->json(["status" => 0, "message" => "Email Already Exists"], 200);
        }
        if ($request->password == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Password"], 200);
        }
        if ($request->cpassword == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Confirm Password"], 200);
        }
        if ($request->password != $request->cpassword) {
            return response()->json(["status" => 0, "message" => "Password and Confrim Password do not match"], 200);
        }

        $otp = rand(1000, 9999);
        $user = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => $request->password,
            'otp' => $otp
        );
        $send_mail = Helper::verificationemail($request->email, $request->name, $otp);

        if ($send_mail == 1) {
            return response()->json(['status' => 1, 'message' => "Email Sent Successfully", 'data' => $user], 200);
        } else {
            return response()->json(['status' => 0, 'message' => "Email Error"], 200);
        }
    }
    public function verify(Request $request)
    {
        $checkemail = User::where('email', $request->email)->first();

        if ($request->name == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Your Name"], 200);
        }
        if ($request->email == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Email"], 200);
        }
        if ($request->phone == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Phone Number"], 200);
        }
        if (!empty($checkemail)) {
            return response()->json(["status" => 0, "message" => "Email Already Exists"], 200);
        }
        if ($request->password == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Password"], 200);
        }
        if ($request->cpassword == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Confirm Password"], 200);
        }
        if ($request->password != $request->cpassword) {
            return response()->json(["status" => 0, "message" => "Password and Confrim Password do not match"], 200);
        }

        $user = new User();
        $user->type = 3;
        $user->login_type = 1;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = Hash::make($request->password);
        $user->is_verified = 1;
        $user->save();

        $checkuser = User::find($user->id);
        $user = array(
            'id' => $checkuser->id,
            'name' => $checkuser->name,
            'email' => $checkuser->email,
            'phone' => $checkuser->phone == "" ? "" : $checkuser->phone,
            'image' => Helper::image_path($checkuser->image),
        );
        return response()->json(['status' => 1, 'message' => 'Sign up Successfilly', 'user_data' => $user], 200);
    }
    public function resend_otp(Request $request)
    {
        $otp = rand(1000, 9999);
        $send_mail = Helper::verificationemail($request->email, $request->name, $otp);

        if ($send_mail == 1) {
            return response()->json(['status' => 1, 'message' => "OTP Sent Successfully On Email", 'otp' => $otp], 200);
        } else {
            return response()->json(['status' => 0, 'message' => "Email Error"], 200);
        }
    }
    public function forgot_password(Request $request)
    {
        $checkemail = User::where('email', $request->email)->first();

        if ($request->email == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Email"], 200);
        }
        if (empty($checkemail)) {
            return response()->json(["status" => 0, "message" => "Invalid Email Address"], 200);
        }
        $password = Str::random(10);
        $send_mail = Helper::forgotpassword($checkemail->email, $checkemail->name, $password);

        if ($send_mail == 1) {
            $checkemail->password = Hash::make($password);
            $checkemail->save();
            return response()->json(['status' => 1, 'message' => "Email Sent Successfully"], 200);
        } else {
            return response()->json(['status' => 0, 'message' => "Email Error"], 200);
        }
    }
    public function changepassword(Request $request)
    {
        if ($request->user_id == "") {
            return response()->json(["status" => 0, "message" => "Please Select Login User ID"], 200);
        }
        if ($request->current_password == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Current Password"], 200);
        }
        if ($request->current_password == $request->new_password) {
            return response()->json(["status" => 0, "message" => "Your New Password Cannot Be The Same As Your Current Password"], 200);
        }
        if ($request->new_password == "") {
            return response()->json(["status" => 0, "message" => "Please Enter New Password"], 200);
        }
        if ($request->confirm_password == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Confirm Password"], 200);
        }
        if ($request->new_password != $request->confirm_password) {
            return response()->json(["status" => 0, "message" => "New Password and Confirm Password Does Not Match"], 200);
        }
        $checkuser = User::find($request->user_id);
        if (!empty($checkuser)) {
            if (Hash::check($request->current_password, $checkuser->password)) {
                $checkuser->password = Hash::make($request->new_password);
                $checkuser->save();
                return response()->json(['status' => 1, 'message' => 'Password Changed Successfully'], 200);
            } else {
                return response()->json(["status" => 0, "message" => "Invalid Current Password"], 200);
            }
        } else {
            return response()->json(["status" => 0, "message" => "Invalid User"], 200);
        }
    }
    public function delete_account(Request $request)
    {
        if ($request->id == "") {
            return response()->json(["status" => 0, "message" => "Please Select Login User ID"], 200);
        }
        $checkuser = User::find($request->id);
        if (!empty($checkuser)) {
            $checkuser->is_deleted = 1;
            $checkuser->save();
            return response()->json(['status' => 1, 'message' => 'Account Deleted Successfully'], 200);
        } else {
            return response()->json(["status" => 0, "message" => "Invalid User ID"], 200);
        }
    }
    public function google_login(Request $request)
    {
        if ($request->email == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Email"], 200);
        }
        $checkuser = User::where('email', $request->email)->where('login_type', 2)->where('is_deleted', 2)->first();
        if (!empty($checkuser)) {
            $userdata = array(
                'id' => $checkuser->id,
                'name' => $checkuser->name,
                'email' => $checkuser->email,
                'phone' => $checkuser->phone == "" ? "" : $checkuser->phone,
                'image' => Str::contains($checkuser->image, 'https') ? $checkuser->image : Helper::image_path($checkuser->image),
            );
            return response()->json(["status" => 1, "message" => "Succesfully Login", 'userdata' => $userdata], 200);
        }
        if ($request->name == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Name"], 200);
        }
        $user = new User;
        $user->type = 3;
        $user->login_type = 2;
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->phone != "") {
            $user->phone = $request->phone;
        }
        $user->google_id = $request->uid;
        if ($request->image != "") {
            $user->image = $request->image;
        }
        $user->is_verified = $request->is_verified == true ? 1 : 2;
        $user->save();
        if (!empty($user)) {
            $userdata = array(
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone == "" ? "" : $user->phone,
                'image' => Str::contains($user->image, 'https') ? $user->image : Helper::image_path($user->image),
            );
            return response()->json(["status" => 1, "message" => "Succesfully Login", 'userdata' => $userdata], 200);
        } else {
            return response()->json(["status" => 0, "message" => "Please Enter Name"], 200);
        }
    }
    public function facebook_login(Request $request)
    {
        if ($request->email == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Email"], 200);
        }
        $checkuser = User::where('email', $request->email)->where('login_type', 4)->where('is_deleted', 2)->first();
        if (!empty($checkuser) && $checkuser->login_type == 4) {
            $userdata = array(
                'id' => $checkuser->id,
                'name' => $checkuser->name,
                'email' => $checkuser->email,
                'phone' => $checkuser->phone == "" ? "" : $checkuser->phone,
                'image' => Str::contains($checkuser->image, 'https') ? $checkuser->image : Helper::image_path($checkuser->image),
            );
            return response()->json(["status" => 1, "message" => "Succesfully Login", 'userdata' => $userdata], 200);
        }
        if ($request->name == "") {
            return response()->json(["status" => 0, "message" => "Please Enter Name"], 200);
        }
        $user = new User;
        $user->type = 3;
        $user->login_type = 4;
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->phone != "") {
            $user->phone = $request->phone;
        }
        $user->facebook_id = $request->uid;
        if ($request->image != "") {
            $user->image = $request->image;
        }
        $user->is_verified = 1;
        $user->save();
        if (!empty($user)) {
            $userdata = array(
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone == "" ? "" : $user->phone,
                'image' => Str::contains($user->image, 'https') ? $user->image : Helper::image_path($user->image),
            );
            return response()->json(["status" => 1, "message" => "Succesfully Login", 'userdata' => $userdata], 200);
        } else {
            return response()->json(["status" => 0, "message" => "Please Enter Name"], 200);
        }
    }
    public function apple_login(Request $request)
    {
    }
}
