<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Domes;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function review(Request $request)
    {
        if ($request->user_id == "") {
            return response()->json(['status' => 0, 'message' => 'Enter Login User ID'], 200);
        }
        if ($request->dome_id == "") {
            return response()->json(['status' => 0, 'message' => 'Enter Dome ID'], 200);
        }
        if ($request->ratting == "") {
            return response()->json(['status' => 0, 'message' => 'Enter Dome Ratting'], 200);
        }
        if ($request->ratting == 1 || $request->ratting == 2 || $request->ratting == 3 || $request->ratting == 4 || $request->ratting == 5) {
            $checkreview = Review::where('dome_id', $request->dome_id)->where('user_id', $request->user_id)->first();
            if (!empty($checkreview)) {
                $checkreview->dome_id = $request->dome_id;
                $checkreview->user_id = $request->user_id;
                $checkreview->ratting = $request->ratting;
                if ($request->comment != "") {
                    $checkreview->comment = $request->comment;
                }
                $checkreview->save();
                return response()->json(['status' => 1, 'message' => 'Review Updated Successfully', 'review' => $checkreview], 200);
            } else {
                $review = new Review;
                $review->dome_id = $request->dome_id;
                $review->user_id = $request->user_id;
                $review->ratting = $request->ratting;
                if ($request->comment != "") {
                    $review->comment = $request->comment;
                }
                $review->save();
                return response()->json(['status' => 1, 'message' => 'Review Submitted Successfully', 'review' => $review], 200);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Invalid Dome Ratting'], 200);
        }
    }
    public function avg_rating(Request $request)
    {
        $checkdome = Domes::find($request->id);
        if ($request->id == "") {
            return response()->json(['status' => 0, 'message' => 'Enter Dome ID'], 200);
        }
        if (!empty($checkdome)) {
            $review = Review::where('dome_id', $request->id)->selectRaw('SUM(ratting)/COUNT(user_id) AS avg_rating')->first()->avg_rating;
            return response()->json(['status' => 1, 'message' => 'Success', 'review' => $review], 200);
        } else {
            return response()->json(['status' => 0, 'message' => 'Dome Not Found'], 200);
        }
    }
}
