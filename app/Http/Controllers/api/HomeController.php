<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\CMS;
use App\Helper\Helper;
use App\Models\Sports;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function privacy_policy(Request $request)
    {
        $policy = CMS::where('type',1)->first();
        return response()->json(['status'=>1,'message'=>'success','policy'=>@$policy->content], 200);
    }
    public function terms_conditions(Request $request)
    {
        $policy = CMS::where('type',2)->first();
        return response()->json(['status'=>1,'message'=>'success','terms_conditions'=>@$policy->content], 200);
    }
    public function sportslist(Request $request)
    {
        $getsportslist = Sports::where('is_available', 1)->where('is_deleted', 2)->get();
        $sportslist = [];
        foreach ($getsportslist as $sports) {
            $sportslist[] = [
                "id" => $sports->id,
                "name"=>$sports->name,
                "image"=>Helper::image_path($sports->image),
            ];
        }
        return response()->json(['status' => 1, 'message' => "Success", 'sportslist' => $sportslist], 200);
    }
}
