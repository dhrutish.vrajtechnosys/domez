<?php

namespace App\Http\Controllers\api;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Sports;
use App\Models\DomeImages;
use App\Models\Domes;
use App\Models\Favourite;
use App\Models\Field;
use App\Models\Review;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DomesController extends Controller
{
    public function domes_list(Request $request)
    {
        if ($request->type != "") {
            if ( in_array($request->type,[1,2,3])) {
                $domes_list = [];
                //Type = 1 (Recent Bookings Dome Data)
                if ($request->type == 1) {
                    if ($request->user_id != "") {
                        if (empty(User::find($request->user_id))) {
                            return response()->json(['status' => 0, 'message' => 'Invalid User ID'], 200);
                        }
                        $bookings = Booking::where('user_id', $request->user_id)->get();
                        foreach ($bookings as $booking) {
                            $dome = Domes::where('id', $booking->dome_id)->where('is_deleted', 2)->first();
                            $dome_image = DomeImages::where('dome_id', $booking->dome_id)->first();
                            $fields = Field::where('dome_id', $booking->dome_id)->where('is_deleted', 2)->count();
                            $categories = explode('|', $dome->sport_id);
                            $categoriess = Sports::whereIn('id', $categories)->where('is_available', 1)->where('is_deleted', 2)->get();
                            foreach ($categoriess as $cat) {
                                $category_data = array(
                                    'category_image' => Helper::image_path($cat->image),

                                );
                                $category_list[] = $category_data;
                            }
                            if (!empty($dome)) {
                                if ($request->user_id != "") {
                                    $is_fav = Favourite::where('dome_id', $dome->id)->where('user_id', $request->user_id)->first();
                                }
                                $dome_data = array(
                                    "id" => $dome->id,
                                    "name" => $dome->name,
                                    "image" => Helper::image_path($dome_image->images),
                                    "price" => $dome->price,
                                    "city" => $dome->city,
                                    "state" => $dome->state,
                                    "total_fields" => $fields,
                                    "is_fav" => !empty($is_fav) ? true : false,
                                    "booking_date" => $booking->booking_date,
                                    // "category_list"=>$category_list
                                );
                            } else {
                                return response()->json(['status' => 0, 'message' => 'Data Not Found'], 200);
                            }
                            $domes_list[] = $dome_data;
                        }
                    } else {
                        return response()->json(['status' => 0, 'message' => 'Enter Login User ID'], 200);
                    }
                }
                //Type = 2 (Most Popular Dome Data)
                if ($request->type == 2) {
                    $popular_domes = Booking::select('dome_id', DB::raw('count(bookings.dome_id)as dome'))->groupBy('dome_id')->orderBy('dome', 'desc')->get();
                    foreach ($popular_domes as $popular_dome) {
                        $dome = Domes::where('id', $popular_dome->dome_id)->where('is_deleted', 2)->first();
                        $dome_image = DomeImages::where('dome_id', $popular_dome->dome_id)->first();
                        $fields = Field::where('dome_id', $popular_dome->dome_id)->where('is_deleted', 2)->count();
                        $categories = explode('|', $dome->sport_id);
                            $categoriess = Sports::whereIn('id', $categories)->where('is_available', 1)->where('is_deleted', 2)->get();
                            foreach ($categoriess as $cat) {
                                $category_data = array(
                                    'category_image' => Helper::image_path($cat->image),

                                );
                                $category_list[] = $category_data;
                            }
                            if (!empty($dome)) {
                                if ($request->user_id != "") {
                                    $is_fav = Favourite::where('dome_id', $dome->id)->where('user_id', $request->user_id)->first();
                                }
                                $dome_data = array(
                                    "id" => $dome->id,
                                    "name" => $dome->name,
                                    "image" => Helper::image_path($dome_image->images),
                                    "price" => $dome->price,
                                    "city" => $dome->city,
                                    "state" => $dome->state,
                                    "total_fields" => $fields,
                                    "is_fav" => !empty($is_fav) ? true : false,
                                    "booking_date" => null,
                                    // "category_list"=>$category_list
                                );
                            } else {
                                return response()->json(['status' => 0, 'message' => 'Data Not Found'], 200);
                            }
                            $domes_list[] = $dome_data;
                    }
                }
                //Type = 3 (Domes Around You)
                if ($request->type == 3) {
                    if ($request->lat == "") {
                        return response()->json(['status' => 0, 'message' => 'Enter Latitude'], 200);
                    }
                    if ($request->lng == "") {
                        return response()->json(['status' => 0, 'message' => 'Enter Longitude'], 200);
                    }
                    $lat = $request->lat;
                    $lng = $request->lng;
                    // The Distance Will Be in Kilometers
                    $getarounddomes = Domes::with('dome_image')->select('domes.*'
                            ,DB::raw("6371 * acos(cos(radians(" . $lat . "))
                            * cos(radians(lat))
                            * cos(radians(lng) - radians(" . $lng . "))
                            + sin(radians(" .$lat. "))
                            * sin(radians(lat))) AS distance"));
                    if ($request->kilometer > 0) {
                        $getarounddomes = $getarounddomes->having('distance','<=',$request->kilometer);
                    }
                    $getarounddomes = $getarounddomes->orderBy('distance')->get();
                    foreach ($getarounddomes as $dome) {
                        if ($request->user_id != "") {
                            $is_fav = Favourite::where('dome_id', $dome->id)->where('user_id', $request->user_id)->first();
                        }
                        $domes_list[] = [
                            "id" => $dome->id,
                            "name" => $dome->name,
                            "image" => $dome->dome_image,
                            "price" => $dome->price,
                            "city" => $dome->city,
                            "state" => $dome->state,
                            "is_fav" => !empty(@$is_fav) ? true : false,
                            "total_fields" => Field::where('dome_id', $dome->id)->where('is_deleted', 2)->count(),
                            "sports_list" => Sports::select('id','name',DB::raw("CONCAT('" . url('storage/app/public/admin/images/sports') . "/', image) AS image"))->whereIn('id', explode('|', $dome->sport_id))->where('is_available', 1)->where('is_deleted', 2)->get(),
                        ];
                    }
                }
                return response()->json(['status' => 1, 'message' => 'Success', 'domes_list' => $domes_list], 200);
            } else {
                return response()->json(['status' => 0, 'message' => 'Invalid Type'], 200);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Enter Dome Data Type'], 200);
        }
    }
    public function domes_details(Request $request)
    {
        $dome_data = $this->getdomedataobject($request->id);
        if ($dome_data != 1) {
            return response()->json(['status' => 1, 'message' => 'Success', 'dome_details' => $dome_data], 200);
        } else {
            return response()->json(['status' => 0, 'message' => 'Dome Not Found'], 200);
        }
    }
    public function getdomedataobject($id)
    {
        $dome = Domes::with('dome_images')->where('id',$id)->first();
        if (empty($dome)) {
            return $dome_data = 1;
        }
        $categories = explode('|', $dome->sport_id);
        $benefits = explode('|', $dome->benefits);


        $categoriess = Sports::select('id','name',DB::raw("CONCAT('" . url('storage/app/public/admin/images/sports') . "/', image) AS image"))->whereIn('id', explode('|', $dome->sport_id))->where('is_available', 1)->where('is_deleted', 2)->get();
        foreach ($categoriess as $cat) {

            $fields = Field::where('dome_id', $id)->whereIn('sport_id', $categories)->where('is_available', 1)->where('is_deleted', 2)->get();

            $field_data = [];
            foreach ($fields as $field) {
                $field_data[] = [
                    'field_id' => $field->id,
                    'field_name' => $field->name,
                    'field_area' => $field->area . ' ' . 'Sqr Ft',
                    'field_person' => $field->min_person . '-' . $field->max_person . ' ' . 'People',
                ];
            }
            $sports_list[] = [
                'sport_id' => $cat->id,
                'sport_name' => $cat->name,
                'sport_image' => $cat->image,
                'field_data' => $field_data,
            ];
        }
        $review = Review::where('dome_id', $dome->id)->selectRaw('SUM(ratting)/COUNT(user_id) AS avg_rating')->first()->avg_rating;
        $total_reviews = Review::where('dome_id', $dome->id)->get();
        if ($total_reviews->count() > 100) {
            $total_review = '100+';
        } else {
            $total_review = $total_reviews->count();
        }
        if (!empty($dome)) {
            $dome_data = array(
                'id' => $dome->id,
                'vendor_id' => $dome->vendor_id,
                'total_fields' => $fields->count(),
                'name' => $dome->name,
                'price' => $dome->price,
                'address' => $dome->address,
                'pin_code' => $dome->pin_code,
                'city' => $dome->city,
                'state' => $dome->state,
                'country' => $dome->country,
                'start_time' => $dome->start_time,
                'end_time' => $dome->end_time,
                'description' => $dome->description,
                'lat' => $dome->lat,
                'lng' => $dome->lng,
                'benefits_description' => $dome->benefits_description,
                'avg_rating' => $review,
                'total_review' => $total_review,
                'benefits' => $benefits,
                'sports_list' => $sports_list,
                'dome_images' => $dome->dome_images,
            );
        }
        return $dome_data;
    }
    public function dome_sport_fields(Request $request)
    {
        if ($request->dome_id == "") {
            return response()->json(['status' => 0, 'message' => 'Enter Dome ID'], 200);
        }
        if ($request->sport_id == "") {
            return response()->json(['status' => 0, 'message' => 'Enter Sport ID'], 200);
        }
        $fields = Field::where('dome_id', $request->dome_id)->where('sport_id', $request->sport_id)->where('is_deleted', 2)->get();
        if (!empty($fields)) {
            $field_list = [];
            foreach ($fields as $field) {
                $field_list[] =  [
                    'field_id' => $field->id,
                    'field_name' => $field->name,
                    'field_area' => $field->area . ' ' . 'Sqr Ft',
                    'field_person' => $field->min_person . '-' . $field->max_person . ' ' . 'People',
                ];
            }
            return response()->json(['status' => 0, 'message' => 'Success', 'field_list' => $field_list], 200);
        } else {
            return response()->json(['status' => 0, 'message' => 'Fields Not Found'], 200);
        }
    }
}
