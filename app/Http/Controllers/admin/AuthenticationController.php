<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Helper\Helper;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Storage;

class AuthenticationController extends Controller
{
    public function index(Request $request)
    {
        return view('new_admin.authentication.login');
    }

    public function checklogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8',
        ], [
            'email.required' => 'Please Enter Email',
            'email.email' => 'Invalid Email Address',
            'password.required' => 'Please Enter Password',
            'password.min' => 'Password must be at least 8 characters in length',
        ]);
        if (Auth::attempt($request->only('email', 'password'))) {
            if (Auth::user()->type == 1) {
                if (Auth::user()->is_verified == 1) {
                    if (Auth::user()->is_available == 1) {
                        if (Auth::user()->is_deleted == 2) {
                            return redirect('admin/dashboard')->with('success', 'Logged In');
                        } else {
                            return redirect()->back()->with('error', 'Account has been deleted');
                        }
                    } else {
                        return redirect()->back()->with('error', 'Account has been inactive');
                    }
                } else {
                    $otp = rand(1000, 9999);
                    $user = User::where('email', Auth::user()->email)->first();
                    $user->otp = $otp;
                    $user->save();
                    $send_mail = Helper::verificationemail($user->email, $user->name, $otp);
                    session()->put('verification_email', Auth::user()->email);
                    if ($send_mail == 1) {
                        return redirect('verification')->with('success', 'Email Sent Successfully');
                    } else {
                        return redirect()->back()->with('error', 'Email Error');
                    }
                }
            } elseif (Auth::user()->type == 2) {
                if (Auth::user()->is_available == 1) {
                    if (Auth::user()->is_verified == 1) {
                        if (Auth::user()->is_deleted == 2) {
                            return redirect('admin/dashboard')->with('success', 'Logged In');
                        } else {
                            return redirect()->back()->with('error', 'Account has been deleted');
                        }
                    } else {
                        $otp = rand(1000, 9999);
                        $user = User::where('email', Auth::user()->email)->first();
                        $user->otp = $otp;
                        $user->save();
                        $send_mail = Helper::verificationemail(Auth::user()->email, $user->name, $otp);
                        session()->put('verification_email', Auth::user()->email);
                        if ($send_mail == 1) {
                            return redirect('verification')->with('success', 'Email Sent Successfully');
                        } else {
                            return redirect()->back()->with('error', 'Email Error');
                        }
                    }
                } else {
                    return redirect()->back()->with('error', 'Account has been inactive');
                }
            }
        } else {
            return redirect()->back()->with('error', 'Email/Password Incorrect');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/')->with('success', 'Logged Out');
    }

    public function register(Request $request)
    {
        return view('new_admin.authentication.register');
    }

    public function store_register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'cpassword' => 'required|same:password'
        ], [
            'name.required' => 'Please Enter Name',
            'email.required' => 'Please Enter Email',
            'email.email' => 'Invalid Email Address',
            'email.unique' => 'This Email is Already Taken',
            'password.required' => 'Please Enter Password',
            'password.min' => 'Password must be at least 8 characters in length',
            'cpassword.required' => 'Please Enter Confirm Password',
            'cpassword.same' => 'Password and Confrim Password do not match',
        ]);

        $vendor = new User;
        $vendor->type = 2;
        $vendor->login_type = 1;
        $vendor->name = $request->name;
        $vendor->email = $request->email;
        $vendor->password = Hash::make($request->password);
        $vendor->save();

        return redirect('/')->with('success', 'Successfully');
    }

    public function verification(Request $request)
    {
        return view('new_admin.authentication.verification');
    }

    public function forgot_password(Request $request)
    {
        return view('new_admin.authentication.forgot_password');
    }

    public function send_pass(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ], [
            'email.required' => 'Please Enter Email Address',
            'email.email' => 'Please Enter Valid Email Address'
        ]);
        $checkuser = User::where('email', $request->email)->first();
        if (!empty($checkuser)) {
            $password = Str::random(10);
            $send_mail = Helper::forgotpassword($checkuser->email, $checkuser->name, $password);
            if ($send_mail == 1) {
                $checkuser->password = Hash::make($password);
                $checkuser->save();
                return redirect('check-mail')->with('success','Email Sent Successfully');
            }
        } else {
            return redirect()->back()->with('error', 'Invalid Email Address');
        }
    }

    public function check_mail(Request $request)
    {
        return view('new_admin.authentication.check_mail');
    }

    public function resend(Request $request)
    {
        $otp = rand(1000, 9999);
        $user = User::where('email', Auth::user()->email)->first();
        $user->otp = $otp;
        $user->save();
        $send_mail = Helper::verificationemail($user->email, $user->name, $otp);
        session()->put('verification_email', Auth::user()->email);
        if ($send_mail == 1) {
            return redirect()->back()->with('success', 'Email Sent Successfully');
        } else {
            return redirect()->back()->with('error', 'Something Went Wrong');
        }
    }

    public function verify(Request $request)
    {
        if (implode("", $request->otp) == "") {
            return redirect()->back()->with('error', 'Please Enter OTP');
        }
        $otp = implode("", $request->otp);
        $user = User::where('email', session('verification_email'))->first();
        if (!empty($user)) {
            if ($user->otp == $otp) {
                session()->forget('verification_email');
                $user->otp = 0;
                $user->is_verified = 1;
                $user->save();

                Auth::login($user);

                return redirect('admin/dashboard')->with('success', 'Successfully');
            } else {
                return redirect()->back()->with('error', 'Invalid OTP');
            }
        } else {
            return redirect('/')->with('error', 'Something Went Wrong');
        }
    }

    public function googleredirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function loginwithgoogle()
    {
        try {
            dd(Socialite::driver('google')->user());
        } catch (\Throwable $th) {
            dd($th);
        }
    }
}
