<?php

use App\Http\Controllers\admin\BookingController;
use App\Http\Controllers\api\AdminController;
use App\Http\Controllers\api\DomesController;
use App\Http\Controllers\api\AuthenticationController;
use App\Http\Controllers\api\FavouriteController;
use App\Http\Controllers\api\HomeController;
use App\Http\Controllers\api\PaymentController;
use App\Http\Controllers\api\ReviewController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('optimize', [AdminController::class, 'optimize']);

Route::post('sign-up', [AuthenticationController::class, 'sign_up']);
Route::post('sign-in', [AuthenticationController::class, 'sign_in']);
Route::post('verify', [AuthenticationController::class, 'verify']);
Route::post('resend-otp', [AuthenticationController::class, 'resend_otp']);
Route::post('forgot-password', [AuthenticationController::class, 'forgot_password']);
Route::post('change-password', [AuthenticationController::class, 'changepassword']);
Route::post('google-login', [AuthenticationController::class, 'google_login']);

Route::get('sportslist', [HomeController::class, 'sportslist']);
Route::get('privacy-policy', [HomeController::class, 'privacy_policy']);
Route::get('terms-conditions', [HomeController::class, 'terms_conditions']);

Route::get('delete-account-{id}', [AuthenticationController::class, 'delete_account']);

Route::post('payment', [PaymentController::class,'payment']);

Route::post('domes-list',[DomesController::class, 'domes_list']);
Route::get('dome-details-{id}',[DomesController::class, 'domes_details']);
Route::post('dome-sport-fields', [DomesController::class, 'dome_sport_fields']);

Route::post('favourite',[FavouriteController::class, 'favourite']);
Route::get('favourite-list-{id}', [FavouriteController::class, 'favourite_list']);

Route::post('booking',[BookingController::class, 'booking']);
Route::post('check-booking',[BookingController::class, 'check_booking']);

Route::post('review', [ReviewController::class, 'review']);
Route::get('avg-ratting-{id}', [ReviewController::class, 'avg_rating']);


