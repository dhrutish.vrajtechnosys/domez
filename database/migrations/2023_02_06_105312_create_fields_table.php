<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->integer('vendor_id');
            $table->integer('dome_id');
            $table->string('sport_id');
            $table->string('name');
            $table->double('area', 8, 2)->default(0);
            $table->integer('min_person');
            $table->integer('max_person');
            $table->string('image');
            $table->tinyInteger('is_available')->default(1)->comment('1=yes,2=no');
            $table->tinyInteger('is_deleted')->default(2)->comment('1=yes,2=no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
};
